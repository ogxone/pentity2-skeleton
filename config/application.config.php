<?php

use Utils\Param\Param;

$envs = ['development', 'qa', 'stage', 'production'];
if (!in_array(ENVIRONMENT, $envs)) {
    die(sprintf('Unknown environment %s only %s allowed', ENVIRONMENT, implode(', ', $envs)));
}
$configs = [];
foreach ($envs as $env) {
    $configs[] = $env;
    if (ENVIRONMENT == $env) {
        break;
    }
}
$configs = implode(',', $configs);
return [
    // This should be an array of module namespaces used in the application.
    'modules' => [
        'index', 'thewall',
        'common', 'mail',
        'build', 'auth',
        'tester',
    ],
    'module_listener_options' => [
        'module_paths' => [
            './module',
            './vendor',
        ],
        'config_glob_paths' => [
            '{config/autoload/{,*.}{' . $configs . '}.php,config/autoload/build/{,*.}{*}.php}',
        ],
        'config_cache_enabled' => false,
        'config_cache_key' => 'app_config',
        'module_map_cache_enabled' => USE_CACHING,
        'module_map_cache_key' => 'module_map',
        'cache_dir' => DATA_PATH . '/cache/config/',
        'check_dependencies' => !USE_CACHING,
    ],
    'service_manager' => [
        'factories' => [
            'App\Translator' => function (\Zend\ServiceManager\ServiceLocatorInterface $sl) {
                $config = $sl->get('Config');
                $trConfig = isset($config['translator']) ? $config['translator'] : [];
                $translator = \Zend\I18n\Translator\Translator::factory($trConfig);
                $translator->setFallbackLocale($config['default_locale']);
                if (false === strpos(php_sapi_name(), 'cli')) { // try autodetect locale if not cli mode
                    $translator->setLocale(\Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']));
                }
                return $translator;
            },
            'App\SessionManager' => function(\Zend\ServiceManager\ServiceLocatorInterface $sl){
                $config = $sl->get('config');

                $sessionStorage = \Zend\Cache\StorageFactory::factory(
                    Param::getArr('session_storage_config', $config)
                );
                $sessionConfig = new \Zend\Session\Config\SessionConfig;
                $sessionConfig->setOptions(
                    Param::getArr('session_config', $config)
                );
                $manager = new \Zend\Session\SessionManager($sessionConfig);
                $manager->setSaveHandler(
                    new \Zend\Session\SaveHandler\Cache($sessionStorage)
                );
                return $manager;
            },
            'App\Acl' => function(\Zend\ServiceManager\ServiceLocatorInterface $sl){
                $config = $sl->get('Config');
                $aclConfig = Param::getArr('acl', $config);
                return new Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl\Acl(
                    Param::getArr('resources', $aclConfig),
                    Param::getArr('roles', $aclConfig),
                    Param::getArr('roles_inheritance', $aclConfig),
                    Param::getArr('rules', $aclConfig)
                );
            },
            'App\CachingAcl' => function(\Zend\ServiceManager\ServiceLocatorInterface $sl){
                //will replace App\Acl if caching mode turned on
                $config = $sl->get('Config');
                $aclConfig = Param::getArr('acl', $config);
                return new \Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl\CachingAcl(
                    Param::getArr('resources', $aclConfig),
                    Param::getArr('roles', $aclConfig),
                    Param::getArr('roles_inheritance', $aclConfig),
                    Param::getArr('rules', $aclConfig),
                    $sl->get('Cache\Acl')
                );
            },
            'App\AuthService' => function(\Zend\ServiceManager\ServiceLocatorInterface $sl){
                $adapter = $sl->get('Db');
                $dbAuthAdapter = new \Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter(
                    $adapter,
                    'users',
                    'username',
                    'password'
                );
                $auth = new \Zend\Authentication\AuthenticationService;
                $auth->setAdapter($dbAuthAdapter);
                return $auth;
            },
            'Aws\S3' => function (\Zend\ServiceManager\ServiceLocatorInterface $sl) {
                $config = $sl->get('Config');
                $awsConfig = empty($config['aws_s3']) ? [] : $config['aws_s3'];
                $aws = Aws\Common\Aws::factory($awsConfig['credentials']);
                return $aws->get('S3');
            },
        ],
        'aliases' => [
            'Zend\Authentication\AuthenticationService' => 'App\AuthService'
        ]
    ]
];
