<?php
$conf = [
    'salt' => "X5CGWBihMI",
    'auth_session_lifetime' => 60 * 60 + 24 * 17, //14 days
    'developers_email' => [
        'velchenko@360lab.com'
    ],
    'php_settings' => [
        'display_errors' => 1,
        'error_reporting' => E_ALL
    ],
    'mail' => [
        'service_email' => 'Pentity2@noreply.com',
        'salt' => 'X5CGWBihMI',
        'activation_link_lifetime' => 60 * 60 + 24 * 7 // 7 days
    ],
    'form' => [
        'salt' => "X5CGWBihMI",
        'csrf_timeout' => 1200,
        'captcha' => [
            'class' => 'image',
            'options' => [
                'imgDir' => PUBLIC_PATH . '/data/captcha',
                'imgDelete' => true,
                'font' => DATA_PATH . '/fonts/arial.ttf',
                'width' => 200,
                'height' => 50,
                'dotNoiseLevel' => 40,
                'lineNoiseLevel' => 3,
                'suffix' => '',
                'messages' => array(
                    'badCaptcha' => 'bad_captcha'
                )
            ],
        ]
    ],
    'acl' => [
        'roles' => [
            'guest',
            'member',
            'editor',
            'admin',
        ],
        'default_role' => 'guest',
        'roles_inheritance' => [
            'member'         => ['guest'],
            'editor'         => ['member'],
        ],
        'resources' => file_exists('acl.php') ? include 'acl.php' : [],
        'rules' => [
            'admin' => [
                'allow' => [
                    null
                ],
                'deny' => [
                    'auth_index_index',
                ]
            ],
            'guest' => [
                'deny' => [
                    null
                ],
                'allow' => [
                    'tester',
                    'build',
                    'auth_index_index',
                    'auth_index_register',
                    'auth_index_generate-captcha',
                    'auth_restore-pass',
                    'index',
                ]
            ],
            'member' => [
                'deny' => [
                    'auth_index_index',
                ],
                'allow' => [
                    'auth_index_logout',
                    'auth_change'
                ]
            ]
        ]
    ],
    'session_config' => [
        'remember_me_seconds' => 2419200,
        'use_cookies' => true,
        'cookie_httponly' => true,
    ],
    'session_storage_config' => [
        'adapter' => 'filesystem',
        'options' => [
            'cache_dir' => DATA_PATH . '/cache/session'
        ],
        'plugins' => [
            'exception_handler' => [
                'throw_exceptions' => false
            ],
            'serializer'
        ]
    ],
    'default_locale' => 'en_US',
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'phparray',
                'base_dir' => DATA_PATH . '/build/langs',
                'pattern' => '%s.php',
            ]
        ]
    ],
    'db' => [
        'adapters' => [
            'Db' => [
                'dsn' => 'mysql:dbname=pentity2;host=172.16.16.16',
                'username' => 'admin',
                'password' => 'MGc1osCE'
            ]
        ]
    ],
    'service_manager' => [
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Db\Adapter\AdapterAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory'
        ],
        'delegators' => [
            'Cache\Db' => [
                '\Pentity2\Infrastructure\Cache\Delegators\MasterSlaveCache'
            ],
            'Cache\Routes' => [
                '\Pentity2\Infrastructure\Cache\Delegators\TaggableCache'
            ],
            'Cache\Pages' => [
                '\Pentity2\Infrastructure\Cache\Delegators\TaggableCache'
            ]
        ],
    ],
    'caches' => [
        'Cache\Translator' => [
            'adapter'    => 'redis',
            'options' => [
                'ttl' => 3600,
                'namespace' => 'Cache\Translator'
            ],
            'plugins' => [
                'exception_handler' => [
                    'throw_exceptions' => false
                ]
            ]
        ],
        'Cache\Acl' => [
            'adapter' => 'filesystem',
            'options' => [
                'cache_dir' => DATA_PATH. '/cache/acl',
                'namespace' => 'Cache\Acl'
            ],
            'plugins' => [
                'exception_handler' => [
                    'throw_exceptions' => false
                ],
                'serializer'
            ]
        ],
        'Cache\Routes' => [
            'adapter' => 'filesystem',
            'options' => [
                'cache_dir' => DATA_PATH . '/cache/routes',
                'namespace' => 'Cache\Routes'
            ],
            'plugins' => [
                'exception_handler' => [
                    'throw_exceptions' => false
                ],
                'serializer'
            ]
        ],
        'Cache\Pages' => [
            'adapter' => 'filesystem',
            'options' => [
                'cache_dir' => DATA_PATH . '/cache/pages',
                'namespace' => 'Cache\Pages'
            ],
            'plugins' => [
                'exception_handler' => [
                    'throw_exceptions' => false
                ],
                'serializer'
            ]
        ],
        'Cache\Db' => [
            'adapter' => 'redis',
            'options' => [
                'server' => [
                    'host' => '127.0.0.1',
                    'port' => '6379',
                    'timeout' => 60
                ],
                'database' => 1,
                'ttl' => 60 * 60 * 24 * 365,
                'namespace' => 'Cache\Db'
            ],
            'plugins' => [
                'exception_handler' => [
                    'throw_exceptions' => false
                ]
            ]
        ],
        'Cache\Repo' => [
            'adapter' => 'memory',
            'options' => [
                'namespace' => 'Cache\Repo'
            ],
            'plugins' => [
                'exception_handler' => [
                    'throw_exceptions' => false
                ]
            ]
        ],
        'Cache\Form' => [
            'adapter' => 'memory',
            'options' => [
                'namespace' => 'Cache\Form'
            ],
            'plugins' => [
                'exception_handler' => [
                    'throw_exceptions' => false
                ]
            ]
        ],
    ],
    'aws_s3' => [
        'bucket' => 'jails',
        'cache' => '315360000', // 10 years
        'cfg' => realpath(__DIR__) . '/..' . "/s3_jails.cfg",
        's3cmd' => '/usr/bin/s3cmd',
        'credentials' => [
            'key'    => 'AKIAJPXQVY5CWWWSIHCQ',
            'secret' => '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',
        ],
        'storage' => [
            'base_path'   => '/motor1_v2/static/',
            'tmp_path'    => '/motor1_v2/static/tmp/',
        ],
        'storage_ui' => [
            'base_path'   => '/motor1_v2/static4ui/',
            'tmp_path'    => '/motor1_v2/static4ui/tmp/',
        ],
        'cdn' => [
            'count'      => 9,
            'base_url'   => 'http://jails.s3.amazonaws.com/motor1_v2/static/',
            'tmp_url'    => 'http://jails.s3.amazonaws.com/motor1_v2/static/tmp/',
        ],
        'cdn_ui' => [
            'base_url'   => 'http://jails.s3.amazonaws.com/motor1_v2/static4ui/',
            'tmp_url'    => 'http://jails.s3.amazonaws.com/motor1_v2/static4ui/tmp/',
        ],
    ]
];
if (defined('PENTITY_DDD_CONFIG_PATH')) {
    $conf = Zend\Stdlib\ArrayUtils::merge(include PENTITY_DDD_CONFIG_PATH, $conf);
}
return $conf;