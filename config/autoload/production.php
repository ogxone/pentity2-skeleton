<?php
return [
    'php_settings' => [
        'display_errors' => 0,
        'date.timezone' => 'UTC'
    ],
//    'session_storage_config' => [
//        'adapter'    => 'redis',
//        'options' => [
//            'ttl' => 3600
//        ],
//        'plugins' => [
//            'exception_handler' => [
//                'throw_exceptions' => false
//            ]
//        ]
//    ],
];