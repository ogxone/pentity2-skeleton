<?php
if (class_exists('Zend\Mvc\Application')) {
    return;
}
$loader = include VENDOR_PATH . '/autoload.php';
if (!file_exists('classmap_built')) {
    include 'autoload_functions.php';
    addModulesPsr4NamespacesConfig($loader);
    addVendorsPsr4NamespacesConfig($loader);
}
if (!class_exists('Zend\Mvc\Application')) {
    throw new RuntimeException('Unable to load ZF2. Run `php composer.phar install` or define a ZF2_PATH environment variable.');
}
