<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.08.15
 * Time: 14:42
 */

function addModulesPsr4NamespacesConfig(\Composer\Autoload\ClassLoader $loader)
{
    $di = new DirectoryIterator(ROOT_PATH . '/module');
    foreach ($di as $module) {
        if ($module->isDot()) {
            continue;
        }
        $loader->addPsr4(ucfirst($module->getBasename()) . '\\', $module->getPathname() . '/src');
    }
}

function addVendorsPsr4NamespacesConfig(\Composer\Autoload\ClassLoader $loader)
{
    $di = new DirectoryIterator(VENDOR_PATH . '/private');
    foreach ($di as $module) {
        if ($module->isDot()) {
            continue;
        }
        $loader->addPsr4(ucfirst($module->getBasename()) . '\\', $module->getPathname() . '/src');
    }
}