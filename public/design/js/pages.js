pages = {
    init: {
        page_auth_index_index: function(){
            $('#auth').validate();
        },
        page_auth_index_register: function(){
            $('#register').validate();
        },
        'page_auth_restore-pass_send-restore-pass-link': function(){
            $('#restore_pass_email').validate();
        },
        'page_auth_restore-pass_check-restore-pass-link': function(){
            $('#restore_pass').validate();
        },
        'page_auth_change_change-pass': function(){
            $('#change_pass').validate();
        },
        'page_auth_change_change-email': function(){
            $('#change_email').validate();
        },
        'page_auth_change_confirm-change-email': function(){
            cmpTrayAlert.init();
            cmpTrayAlert.show(i18n.email_changed);
            goToUrl('/');
        }
    },
    rebuild: {

    },
    remove: {

    }
};

forms = {
    config: {
    },
    statusFalse: {

    },
    statusTrue: {

    }
};
buttons = {
    request: cmpButtonsUtils.buttons.request,
    requestC: cmpButtonsUtils.buttons.requestC,
    requestM: cmpButtonsUtils.buttons.requestM,
    requestMC: cmpButtonsUtils.buttons.requestMC,
    select: cmpButtonsUtils.buttons.select,
    selectAll: cmpButtonsUtils.buttons.selectAll,
    headerMenuToggle: utils.headerMenuToggle,
    footerMenuToggle: utils.footerMenuToggle,
    dateCalendarClear: utils.dateCalendarClear
}