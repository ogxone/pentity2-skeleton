$(function(){
    utils.scaleFix();
    ajaxNavigation.init({
        receivedAsJSON: function(pageId, buildType, href, options, jqXHR){
            var json = $.parseJSON(jqXHR.responseText);
            cmpTrayAlert.show(json.message || i18n.m1034, json.status ? 'message' : 'error', json.status ? 10 : 30);
        },
        beforePageBuild: function(oldPageId, buildType, href){
            //if you need to cleanse previous page
            if(buildType != 'firstBoot' && pages.remove[oldPageId] && typeof pages.remove[oldPageId] == 'function'){
                pages.remove[oldPageId].call(this, oldPageId, buildType, href);
            }
        },
        afterPageBuild: function(oldPageId, newPageId, buildType, href){
            //init page
            if(buildType == 'rebuild'){
                if(pages.rebuild[newPageId] && typeof pages.rebuild[newPageId] == 'function'){
                    pages.rebuild[newPageId].call(this, oldPageId, newPageId, buildType, href);
                }
            }else{
                $(document.body).removeClass('headerMenuOpen footerMenuOpen');
                if(pages.init[newPageId] && typeof pages.init[newPageId] == 'function'){
                    pages.init[newPageId].call(this, oldPageId, newPageId, buildType, href);
                }
            }
        },
        requestError: function(buildType, href, options, jqXHR){
            cmpTrayAlert.show(i18n.m1034, 'error', 30);
        }
    });
    cmpTrayAlert.init({insert:1});
    cmpDialog.init({
        buttonOk: i18n.button_ok,
        buttonCancel: i18n.button_cancel,
        buttonClose: i18n.button_close
    });
    cmpButtons.init({
        buttons: buttons,
        error: function(data, element, response, textStatus, jqXHR){
            cmpTrayAlert.show(i18n.m1034, 'error', 30);
        }
    });
    cmpButtonsToggle.init();
});