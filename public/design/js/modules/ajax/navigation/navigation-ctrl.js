/*####### navigation-ctrl.js - v1.0 - 24.07.2013 #######*/

var ajaxNavigationCtrl = (function(){
    return {
        config: {
            keyCodes: [17,91,93,224]
        },
        init: function(config){
            if(ajaxNavigation){
                if(config && config.constructor === Object){
                    for(var i in config){
                        this.config[i] = config[i];
                    }
                }
                this.listener();
            }
        },
        listener: function(event){
            var _this = this;
            $(document.body)
                .on('keydown', function(event){
                    if(_this.config.keyCodes.length && $.inArray(event.keyCode, _this.config.keyCodes) >= 0){
                        ajaxNavigation.stop();
                    }
                })
                .on('keyup', function(event){
                    if(_this.config.keyCodes.length && $.inArray(event.keyCode, _this.config.keyCodes) >= 0){
                        ajaxNavigation.run();
                    }
                });
        }
    }
})();