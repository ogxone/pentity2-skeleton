/*####### navigation.js - v1.081 - 14.01.2014 #######*/

var ajaxNavigation = (function (){
    return {
        config: {
            domain: '',
                receivedAsJSON: false,
                receivedStatus404: false,
                requestError: false,
                requestBeforeSend: false,
                beforePageBuild: false,
                afterPageBuild: false,
                stopPage: false,
                reloadingScript: true,
                reloadingScriptCache: true
        },
        init: function(config){
            if(config && config.constructor === Object){
                for(var i in config){
                    this.config[i] = config[i];
                }
            }
            if(typeof history.pushState == 'function'){
                this.stoped = false;
                this.listener();
                this.historyProvider();
            }
            this.htmlProvider(document.location.pathname + document.location.search, {build: 'firstBoot'});

        },
        page: {
            firstBoot: true,
                elementId: 0,
                scripts: []
        },
        historyProvider: function(){
            var _this = this;
            window.onpopstate = function(event) {
                if(_this.page.firstBoot){
                    _this.page.firstBoot = false;
                    return;
                }
                _this.htmlProvider(document.location.pathname + document.location.search, event.state, true);
            };
        },
        paused: false,
            pause: function(){
        this.paused = true;
    },
        stoped: true,
            stop: function(){
        this.stoped = true;
    },
        run: function(){
            this.stoped = false;
        },
        listener: function(){
            var selector = 'a[href^="/"]',
                _this = this;
            $(document.body)
                .on('click', selector, function(event) {
                    _this.page.firstBoot = false;
                    if(_this.paused){
                        _this.paused = false;
                        return;
                    }
                    if(_this.stoped){
                        return;
                    }
                    var $this = $(this),
                        href = $this.attr('href'),
                        data = $this.data();

                    if(_this.config.stopPage && typeof _this.config.stopPage == 'function'){
                        var fn = _this.config.stopPage.call(_this, $this, data, href);
                        if(fn === false){
                            event.preventDefault();
                            return false;
                        }
                    };

                    if($this.is('[rel]') || $this.is('[target]')){
                        return true;
                    }else if($this.is('.disabled') || $this.is('.wait')){
                        event.preventDefault();
                    }else{
                        event.preventDefault();
                        var d = new Date();
                        _this.page.elementId = d.getTime();

                        $this.attr('data-id', _this.page.elementId);

                        var options = {
                            elementId: _this.page.elementId,
                            build: data.build,
                            data: data
                        };

                        var d = window.location.protocol + '//' + window.location.host;
                        var url = window.location.href.replace(d, '');
                        if(url != href){
                            history.pushState(options, null, href);
                        }
                        _this.htmlProvider(href, options);
                    }
                });
        },
        htmlProvider: function(href, options, popstate){
            var options = options || {};
            opt = {
                elementId: options.elementId || 0,
                build: options.build || 'build',
                data: options.data || {}
            }
            if(this.htmlProviders[opt.build] && typeof this.htmlProviders[opt.build] == 'function'){
                this.htmlProviders[opt.build].call(this, href, opt, popstate);
            }else{
                this.htmlProviders['build'].call(this, href, opt, popstate);
            }
        },
        $getPage: function(){
            return $('[data-role="page"]').first();
        },
        buildAction: function(newPageId, $root, $page, href, options){
            $root.attr('id', newPageId);

            var newPageClass = $page.attr('class');
            if(!newPageClass){
                newPageClass = '';
            }
            $root.attr('class', newPageClass);

            var eldata = $page.data();

            if(eldata.url){
                $root.attr('data-url', eldata.url);
            }else if(eldata.url == ''){
                $root.attr('data-url', href);
            }else{
                $root.removeAttr('data-url');
            }
            if(eldata.scrollTop && options.data){
                options.data.scrollTop = eldata.scrollTop;
            }
            $root.html($page.html());
            if(options.data && "preventScrolling" in options.data){
                return;
            }
            this.scrollDocument(null, href, options);
        },
        htmlProviders: {
            firstBoot: function(href, options){
                var _this = this,
                    pageId = _this.$getPage().attr('id'),
                    newPageId = pageId,
                    build = options.build,
                    jqXHR = null;
                if(_this.config.reloadingScript){
                    $('script[src]').each(function(){
                        var url = $(this).attr('src');
                        if(url){
                            _this.page.scripts.push(url.replace(/\?.*/ig, ''));
                        }
                    });
                }
                if(_this.config.beforePageBuild && typeof _this.config.beforePageBuild == 'function'){
                    _this.config.beforePageBuild.call(_this, pageId, build, href, options, jqXHR);
                }
                if(_this.config.afterPageBuild && typeof _this.config.afterPageBuild == 'function'){
                    _this.config.afterPageBuild.call(_this, pageId, newPageId, build, href, options, jqXHR)
                }
            },
            build: function(href, options){

                this.defaultHtmlProvider(href, options, function(html, pageId, build, href, options, jqXHR){
                    var newPageId = 0;

                    var $temp = $('<div>');
                    $temp.html(html);

                    var $page = $temp.find('[data-role="page"]').first(),
                        $root = $('#' + pageId);

                    if($root.length && $page.length){
                        newPageId = $page.attr('id');

                        if(newPageId){
                            this.buildAction.call(this, newPageId, $root, $page, href, options);
                        }

                    }
                    $temp.remove();
                    return [newPageId, 'build'];
                });
            },
            rebuild: function(href, options, popstate){
                this.defaultHtmlProvider(href, options, function(html, pageId,  build, href, options, jqXHR){
                    var newPageId = 0;

                    var $temp = $('<div>');
                    $temp.html(html);

                    var $page = $temp.find('[data-role="page"]').first(),
                        $root = $('#' + pageId);

                    if($root.length && $page.length){
                        newPageId = $page.attr('id');

                        if(newPageId){
                            var build = 'rebuild';
                            if(newPageId == pageId && !popstate){
                                var $appends = $page.find('[data-box="append"]'),
                                    $prepends = $page.find('[data-box="prepend"]'),
                                    $replaces = $page.find('[data-box="replace"]');

                                if($replaces.length){
                                    $replaces.each(function(){
                                        var $this = $(this),
                                            id = $this.attr('id');
                                        if(id){
                                            $root.find('#' + id).html($this.html());
                                        }
                                    });
                                }
                                if($appends.length){
                                    $appends.each(function(){
                                        var $this = $(this),
                                            id = $this.attr('id');
                                        if(id){
                                            $root.find('#' + id).append($this.html());
                                        }
                                    });
                                }
                                if($prepends.length){
                                    $prepends.each(function(){
                                        var $this = $(this),
                                            id = $this.attr('id');
                                        if(id){
                                            $root.find('#' + id).prepend($this.html());
                                        }
                                    });
                                }

                            }else{
                                build = 'build';
                                this.buildAction.call(this, newPageId, $root, $page, href, options);
                            }
                        }

                    }
                    $temp.remove();
                    return [newPageId, build];
                });
            }
        },
        defaultHtmlProvider: function(href, options, handler){
            var _this = this,
                build = options.build;

            var ajaxOptions = {
                url: _this.config.domain + href,
                dataType: 'html',
                success: function(html, textStatus, jqXHR){

                    var pageId = _this.$getPage().attr('id');
                    if(jqXHR.getResponseHeader('Content-Type').indexOf('application/json') != -1){
                        if(_this.config.receivedAsJSON && typeof _this.config.receivedAsJSON == 'function'){
                            _this.config.receivedAsJSON.call(_this, pageId, build, href, options, jqXHR)
                        }
                        return;
                    }
                    if(!pageId){
                        return;
                    }

                    function pageProcessing(html, pageId, build, href, options, jqXHR){
                        var title = _this.getTitle(html),
                            html = _this.cleanHTML(html);

                        if(_this.config.beforePageBuild && typeof _this.config.beforePageBuild == 'function'){
                            _this.config.beforePageBuild.call(_this, pageId, build, href, options, jqXHR)
                        }

                        _this.setTitle(title);

                        var h = handler.call(_this, html, pageId, build, href, options, jqXHR);

                        if(_this.config.afterPageBuild && typeof _this.config.afterPageBuild == 'function'){
                            _this.config.afterPageBuild.call(_this, pageId, h[0], h[1], href, options, jqXHR);
                        }
                    }
                    if(_this.config.reloadingScript){
                        var pageScripts = [];
                        var regex = RegExp("<script?\\w+(?:\\s+(?:src=\"([^\"]*)\")|[^\\s>]+|\\s+)*>","gi");
                        var matches;
                        while(matches = regex.exec(html)){
                            if(matches[1]){
                                pageScripts.push(matches[1].replace(/\?.*/ig, ''));
                            }
                        }

                        var diffScripts = $(pageScripts).not(_this.page.scripts).get();

                        if(diffScripts.length){
                            var loadArr = [],
                                diffArr = [];
                            for(var i = 0, c = diffScripts.length; i < c; i++){
                                loadArr.push($.ajax({
                                    url: diffScripts[i],
                                    dataType: 'script',
                                    cache: _this.config.reloadingScriptCache
                                }).done(function(){
                                    diffArr.push(this.url.replace(/\?.*/ig, ''));
                                }));
                            }
                            $.when.apply($, loadArr)
                                .always(function(){
                                    _this.page.scripts = _this.page.scripts.concat(diffArr);
                                    pageProcessing(html, pageId, build, href, options, jqXHR);
                                });
                        }else{
                            pageProcessing(html, pageId, build, href, options, jqXHR);
                        }
                    }else{
                        pageProcessing(html, pageId, build, href, options, jqXHR);
                    }

                },
                error: function(jqXHR){
                    if(jqXHR.status == 404){
                        this.success.call(this, jqXHR.responseText, jqXHR.textStatus, jqXHR);
                        if(_this.config.receivedStatus404 && typeof _this.config.receivedStatus404 == 'function'){
                            _this.config.receivedStatus404.call(_this, build, href, options, jqXHR);
                        }
                    }else if(_this.config.requestError && typeof _this.config.requestError == 'function'){
                        _this.config.requestError.call(_this, build, href, options, jqXHR);
                    }
                },
                beforeSend: function(jqXHR, settings){
                    function addWait(){
                        if(options.elementId){
                            var $el = $('[data-id="' + options.elementId + '"]');
                            if($el.length){
                                $el.addClass('wait');
                            }
                        }
                    }
                    if(_this.config.requestBeforeSend && typeof _this.config.requestBeforeSend == 'function'){
                        var fn = _this.config.requestBeforeSend.call(_this, build, href, options, jqXHR);
                        if(fn === false){
                            return false;
                        }else{
                            addWait();
                        }
                    }else{
                        addWait();
                    }
                },
                complete: function(){
                    if(options.elementId){
                        var $el = $('[data-id="' + options.elementId + '"]');
                        if($el.length){
                            $el.removeClass('wait');
                        }
                    }
                }
            };
            if(options.data && options.data.extraParams){
                var params = options.data.extraParams;
                if (params.indexOf( '{' ) < 0){
                    params = "{" + params + "}";
                }
                ajaxOptions.data = eval("(" + params + ")");
            }

            $.ajax(ajaxOptions);

        },
        scrollDocument: function(id, href, options){
            var fragment = id || document.location.hash.replace(/[^a-zA-Z_0-9-]/g, ''),
                top = 0;
            if(options.data && options.data.scrollTop){
                top = options.data.scrollTop;
            }
            if(fragment){
                var $tel = $('#' + fragment);
                if($tel.length){
                    $(document).scrollTop($tel.offset().top);
                }else{
                    $(document).scrollTop(top);
                }
            }else{
                $(document).scrollTop(top);
            }
        },
        cleanHTML: function(html){
            var html = html.replace(/<head>[\s\S]*?<\/head>/gim, '');
            html = html.replace(/<script[\s\S]*?<\/script>/gim, '');
            return html;
        },
        getTitle: function(html){
            var $title = $('<textarea></textarea>'),
                title = html.match( /<title[^>]*>([^<]*)/ ) && RegExp.$1 || '',
                newTitle = '';
            if(title){
                $title.html(title);
                newTitle = $title.text();
            }
            $title.remove();
            return newTitle;
        },
        setTitle: function(title){
            document.title = title || '';
        }

    }
})();
function goToUrl(href, build, options){
    if(ajaxNavigation.stoped || ajaxNavigation.paused || build === false){
        window.location.href = href;
    }else if(typeof history.pushState == 'function'){
        var d = window.location.protocol + '//' + window.location.host;
        if(new RegExp(d).test(href)){
            href = href.replace(d, '');
        }
        var url = window.location.href.replace(d, '');
        if(url != href){
            history.pushState({}, null, href);
        }
        options = options && options.constructor === Object ? options : {} ;

        options.build = build !== undefined ? build : 'build';
        ajaxNavigation.page.firstBoot = false;
        ajaxNavigation.htmlProvider(href, options);
    }else{
        window.location.href = href;
    }
    return href;
}