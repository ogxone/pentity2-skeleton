/*####### cmp-tray-alert.js - v1.0 - 5.08.2013 #######*/

var cmpTrayAlert = (function(){
    return {
        cls: 'cmpTrayAlert',
            $tray: [],
        config: {
        defaultType: 'message',
            defaultDelay: 10,
            fade: 0,
            insert: 0,
            selector: 'body'
    },
        init: function(config){

            if(config && config.constructor === Object){
                for(var i in config){
                    this.config[i] = config[i];
                }
            }
            this.$tray = $('.' + this.cls);
            if(!this.$tray.length){
                this.$tray = $('<div>').addClass(this.cls).appendTo(this.config.selector);
            }
            this.listener();

        },
        listener: function(){
            var _this = this;
            $(this.config.selector).on('click', '[data-tray-alert]', function(event){
                var $this = $(this),
                    data = $this.data();
                _this.show.call(_this, data.trayAlert, data.trayAlertType, data.trayAlertDelay, data.trayAlertCallback, $this);
            })
                .on('click', '[data-tray-alert-hide]', function(event){
                    _this.remove.call(_this, $(this).parent('.item'));
                });
        },
        tmpMessage: '',
            show: function(message, type, delay, callback, $target){

        var _this = this,
            type = type || this.config.defaultType,
            delay = delay || this.config.defaultDelay,
            t = message + type + delay + '';

        if(t == _this.tmpMessage){
            return;
        }
        this.tmpMessage = t;

        var $close = $('<div class="close" data-key-group="cmp-tray-alert" data-tray-alert-hide></div>');

        var $item = $('<div>')
            .addClass(type)
            .html('<div class="text">' + message + '</div>')
            .fadeIn(this.config.fade, function(){
                var $this = $(this).addClass('item');
                setTimeout(function(){
                    $this.addClass('visible');
                    if(callback && typeof callback == 'function'){
                        callback.call(_this, $this, $target);
                    }else if(callback && typeof callback == 'string'){
                        try{
                            eval(callback + '.call(_this, $this, $target)');
                        }catch(e){

                        }
                    }
                },100);
            })
            .delay(delay * 1000)
            .show(0, function(){
                $(this).addClass('deleted');
            })
            .delay(1000)
            .fadeOut(this.config.fade, function(){
                _this.remove.call(_this, $(this));
            });

        $close.appendTo($item);
        if(this.config.insert){
            $item.prependTo(this.$tray);
        }else{
            $item.appendTo(this.$tray);
        }
        return $item;
    },
        remove: function($el){
            if($el.length){
                $el.remove();
            }
            this.tmpMessage = '';
        }
    }
})();