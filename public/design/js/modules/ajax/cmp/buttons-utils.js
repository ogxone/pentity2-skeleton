/*####### cmp-buttons-utils.js - v1.093 - 25.06.2014 #######*/
var cmpButtonsUtils = (function(){
 return   {
        responseAlertMessages: function(response, type, delay){
            var type = type || (response.status ? 'message' : 'error'),
                delay = delay || (response.status ? 10 : 20);
            if(response.message){
                cmpTrayAlert.show(response.message, type, delay);
            } else if(response.messages && response.messages.length){
                var msg = '<ul>';
                for(var i = 0, c = response.messages.length; i < c; i++){
                    msg += '<li>' + response.messages[i] + '</li>';
                }
                msg += '</ul>';
                cmpTrayAlert.show(msg, type, delay);
            }
        },
        toggleButtons: function(element, data){
            if(data.groupToggle !== undefined || data.multiGroupToggle !== undefined){
                if(this.toggleButtons){
                    this.toggleButtons(element, data);
                }
            }
        },
        requestConfirm: function(element){
            var $this = $(element),
                data = $this.data(),
                ready = data.ready;
            if(!ready){
                cmpDialog.confirm(data.confirmMessage || 'Are you sure?', null, function(value){
                    if(value){
                        $this.data('ready', 1);
                        $this.trigger('click');
                    }
                });
                return false;
            }
            $this.data('ready', 0);
        },
        redirect: function(data, response){
            if('goToUrl' in window){
                var URL = false;
                if("url" in response){
                    URL = response.url;
                }else if('redirectUrl' in data){
                }else if('redirectUrl' in data){
                    URL = data.redirectUrl;
                }else if('redirectSuccessUrl' in data && 'status' in response && response.status){
                    URL = data.redirectSuccessUrl;
                }else if('redirectFailureUrl' in data && 'status' in response && !response.status){
                    URL = data.redirectFailureUrl;
                }
                if(URL !== false){
                    if(URL && URL.length){
                        goToUrl(URL);
                    }else{
                        goToUrl(window.location.href, null, {data: {preventScrolling: ""}});
                    }
                }
            }
        },
        collectSelectedValues: function(data, element){
            var _this = this,
                _data = data;
            if(data && data.value){
                var $selected = $('[data-button="' + data.value + '"].selected:not([data-id])');
                if($selected.length){
                    var values = []
                    $selected.each(function(){
                        var data = $(this).data();
                        if("value" in data){
                            values.push(data.value);
                        }
                    });
                    if(values.length){
                        if(this.options && 'value' in this.options){
                            this.options.value = values;
                        }
                    }
                }
            }
        },
        buttons: {
            statusTrue: function(data, element, response, textStatus, jqXHR){
                cmpButtonsUtils.toggleButtons.call(this, element, data);
                cmpButtonsUtils.responseAlertMessages(response);
                cmpButtonsUtils.redirect(data, response);
            },
            statusFalse: function(data, element, response, textStatus, jqXHR){
                cmpButtonsUtils.responseAlertMessages(response, 'error', 30);
                cmpButtonsUtils.redirect(data, response);
            },
            request: {
                statusTrue: function(data, element, response, textStatus, jqXHR){
                    cmpButtonsUtils.buttons.statusTrue.call(this, data, element, response, textStatus, jqXHR);
                },
                statusFalse: function(data, element, response, textStatus, jqXHR){
                    cmpButtonsUtils.buttons.statusFalse.call(this, data, element, response, textStatus, jqXHR);
                }
            },
            requestC: {
                beforeSend: function(data, element){
                    return cmpButtonsUtils.requestConfirm(element);
                },
                statusTrue: function(data, element, response, textStatus, jqXHR){
                    cmpButtonsUtils.buttons.statusTrue.call(this, data, element, response, textStatus, jqXHR);
                },
                statusFalse: function(data, element, response, textStatus, jqXHR){
                    cmpButtonsUtils.buttons.statusFalse.call(this, data, element, response, textStatus, jqXHR);
                }
            },
            requestM: {
                click: function(data, element){
                    cmpButtonsUtils.collectSelectedValues.call(this, data, element);
                },
                beforeSend: function(data){
                    if(data.value && data.value.constructor === Array){
                        return true;
                    }
                    if(data.uncheckedMessage){
                        cmpTrayAlert.show(data.uncheckedMessage, null, 5);
                    }
                    return false;
                },
                statusTrue: function(data, element, response, textStatus, jqXHR){
                    cmpButtonsUtils.buttons.statusTrue.call(this, data, element, response, textStatus, jqXHR);
                },
                statusFalse: function(data, element, response, textStatus, jqXHR){
                    cmpButtonsUtils.buttons.statusFalse.call(this, data, element, response, textStatus, jqXHR);
                }
            },
            requestMC: {
                click: function(data, element){
                    cmpButtonsUtils.collectSelectedValues.call(this, data, element);
                },
                beforeSend: function(data, element){
                    if(data.value && data.value.constructor === Array){
                        return cmpButtonsUtils.requestConfirm(element);
                    }
                    if(data.uncheckedMessage){
                        cmpTrayAlert.show(data.uncheckedMessage, null, 5);
                    }
                    return false;
                },
                statusTrue: function(data, element, response, textStatus, jqXHR){
                    cmpButtonsUtils.buttons.statusTrue.call(this, data, element, response, textStatus, jqXHR);
                },
                statusFalse: function(data, element, response, textStatus, jqXHR){
                    cmpButtonsUtils.buttons.statusFalse.call(this, data, element, response, textStatus, jqXHR);
                }
            },
            select: function(data, element){
                $(element).toggleClass('selected');
                var $selectAll = $('[data-button][data-value="' + data.button + '"]'),
                    $selectBox = $('[data-select-toggle-box="' + data.button + '"]'),
                    $selected = $('[data-button="' + data.button + '"].selected');
                if($selectAll.length){
                    if($selected.length){
                        $selectAll.filter('.select').removeClass('active').addClass('disabled');
                        $selectAll.filter('.deselect').removeClass('disabled').addClass('active');
                    }else{
                        $selectAll.filter('.deselect').removeClass('active').addClass('disabled');
                        $selectAll.filter('.select').removeClass('disabled').addClass('active');
                    }
                }
                if($selectBox.length){
                    if($selected.length){
                        $selectBox.removeClass('disabled').addClass('active');
                    }else{
                        $selectBox.removeClass('active').addClass('disabled');
                    }
                }
            },
            selectAll: function(data, element){
                if(data && data.value){
                    var $selected = $('[data-button="' + data.value + '"]:not([data-id])'),
                        $selectBox = $('[data-select-toggle-box="' + data.value + '"]');
                    if($selected.length){
                        if(this.toggleButtons){
                            this.toggleButtons(element, data);
                        }
                        if($(element).is('.select')){
                            $selected.addClass('selected');
                        }else{
                            $selected.removeClass('selected');
                        }
                    }
                    if($selectBox.length){
                        var $selected = $('[data-button="' + data.value + '"].selected');
                        if($selected.length){
                            $selectBox.removeClass('disabled').addClass('active');
                        }else{
                            $selectBox.removeClass('active').addClass('disabled');
                        }
                    }
                }
            },
            toggleBox: {
                click: function(data, element){
                    var $element = $(element);
                    if('value' in data){
                        var $target = $('[data-button-target="' + data.value + '"]');
                        if(data.toggle){
                            $element.removeClass('active');
                            $element.data('toggle', false);
                            $target.removeClass('enabled done').addClass('disabled');
                        }else{
                            if(data.clear == 1){
                                $('[data-button][data-id="toggleBox"]').removeClass('active').data('toggle', false);
                                $('[data-button-target]').removeClass('enabled done').addClass('disabled');
                            }else if(data.clear == 2){
                                $('[data-button][data-id="toggleBox"]').removeClass('active').data('toggle', false);
                                $('[data-button-target]').removeClass('enabled done').addClass('disabled');
                                $(document.body)
                                    .on('click.clear-togle-box-' + data.value, function(event){
                                        $('[data-button][data-id="toggleBox"][data-value="' + data.value + '"]').removeClass('active').data('toggle', false);
                                        $('[data-button-target="' + data.value + '"]').removeClass('enabled done').addClass('disabled');
                                        $(this).off('click.clear-togle-box-' + data.value);
                                    });
                            }else if(data.clear == 3){
                                $(document.body)
                                    .on('click.clear-togle-box-' + data.value, function(event){
                                        $('[data-button][data-id="toggleBox"][data-value="' + data.value + '"]').removeClass('active').data('toggle', false);
                                        $('[data-button-target="' + data.value + '"]').removeClass('enabled done').addClass('disabled');
                                        $(this).off('click.clear-togle-box-' + data.value);
                                    });
                            }

                            $element.addClass('active');
                            $element.data('toggle', true);
                            $target.removeClass('disabled').addClass('enabled');
                            if('delay' in data){
                                var delay = parseFloat(data.delay),
                                    t = false;
                                if(!isNaN(delay)){
                                    clearTimeout(t);
                                    t = setTimeout(function(){
                                        if($target.is('.enabled')){
                                            $target.addClass('done');
                                        }
                                    }, delay*1000);
                                }
                            }
                        }
                    }
                },
                blur: function(data, element){
                    if('blur' in data && 'value' in data){
                        var $element = $(element);
                        setTimeout(function(){
                            $element.removeClass('active');
                            $element.data('toggle', false);
                            $('[data-button-target="' + data.value + '"]').removeClass('enabled done').addClass('disabled');
                        }, 300);
                    }
                }
            }
        }
    }
})();