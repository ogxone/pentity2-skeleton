/*####### jquery-validate-config.js - v1.064 - 15.01.2014 #######*/
$.validator.setDefaults({
    onkeyup: false,
    focusCleanup: false,
    focusInvalid: true,
    errorPlacement: function($error, $element) {
        var $parent = $element.parent();
        if($parent.is('label')){
            var $err = $parent.next();
            if($err.is('.error')){
                $err.html($error);
            }else{
                $error.insertAfter($parent);
            }
        }else{
            $error.insertAfter($element);
        }
        if($element.is('.bubbleError')){
            $error.wrap('<div class="errorBubble"></div>');
        }
    },
    errorElement: 'div',
    submitHandler: function(){
        
        var validator = this,
            $form = $(validator.currentForm);
        if($form.attr('method').toLowerCase() == 'get'){
            var a = [],
                $elements = $form.find("input, select, textarea").not(':submit, :reset, :image, [disabled], [name="send"], [data-ignor]');

            $elements.each(function() {
                var n = this.name;
                if (!n) {
                    return;
                }
                var v = $.fieldValue(this);
                if (v && v.constructor == Array) {
                    for (var i=0,max=v.length; i < max; i++) {
                        a.push({name: n, value: v[i]});
                    }
                }
                else if (v !== "" && v !== null && typeof v != 'undefined') {
                    a.push({name: this.name, value: v});
                }
            });
            var url = $form.attr('action');
            if(url){
                if(a.length){
                    var qs = $.param(a);
                    url = url.indexOf('?') < 0 ? url + '?' + qs : url + '&' + qs;
                }
                if('goToUrl' in window){
                    var $submit = $(validator.submitButton), //$form.find(':submit'),
                        d = new Date(),
                        elementId = d.getTime();
                    $submit.attr('data-id', elementId);                  
                    goToUrl(url, null, {data: {preventScrolling: ""}, elementId: elementId});
                }else{
                    window.location = url;
                }
            }

        }else{
            var $submit = $(validator.submitButton), //$form.find(':submit')
                formId = $form.attr('id'),
                config = (forms && forms.config && forms.config[formId] ? forms.config[formId] : {}),
                options = {};


            if($submit.is('.wait') || $submit.is('.disabled')){
                return false;
            }
            options.beforeSubmit = config.beforeSubmit && typeof config.beforeSubmit == 'function' ? config.beforeSubmit : function(){
                $submit.addClass('wait');
            }

            options.success = (config.success && typeof config.success == 'function' ? config.success : function(response){
                $submit.removeClass('wait');
                if(response && response.constructor === Object){
                    var messageType = 'message',
                        delay = 10;
                    if(response.status){
                        if(forms && forms.statusTrue && forms.statusTrue[formId] && typeof forms.statusTrue[formId] == 'function'){
                            forms.statusTrue[formId].call(this, $form, validator, response);
                        } 
                    }else{
                        messageType = 'error';
                        delay = 20;
                        if(forms && forms.statusFalse && forms.statusFalse[formId] && typeof forms.statusFalse[formId] == 'function'){
                            forms.statusFalse[formId].call(this, $form, validator, response);
                        } 
                    }
                    if(response.message){
                        cmpTrayAlert.show(response.message, messageType, delay);
                    } else if(response.messages && response.messages.length){
                        var msg = '<ul>';
                        for(var i = 0, c = response.messages.length; i < c; i++){
                            msg += '<li>' + response.messages[i] + '</li>';
                        }
                        msg += '</ul>';
                        cmpTrayAlert.show(msg, messageType, delay);
                    }
                    if("url" in response && 'goToUrl' in window){
                        if(response.url.length){
                            goToUrl(response.url);
                        }else{
                            goToUrl(window.location.href, null, {data: {preventScrolling: ""}});
                        }
                    }
                }else{
                    cmpTrayAlert.show('i18n' in window ? i18n.m1034 : 'Error', 'error', 30);
                }
            }); 
            options.error = function(){
                $submit.removeClass('wait');
            }
            options.cache = config.cache || false;
            if(config.dataType){
                options.dataType = config.dataType;
            }
            if(config.data){
                options.data = config.data;
            }else{
                options.data = {};
            }
            
            if(config.clearForm){
                options.clearForm = config.clearForm;
            }
            if(config.error && typeof config.error == 'function'){
                options.error = config.error;
            }
            if(config.resetForm){
                options.resetForm = config.resetForm;
            }
            if(config.type){
                options.type = config.type;
            }
            if(config.url){
                options.url = config.url;
            }
            if($submit.data('formClose')){
                options.data.formClose = 1;
            }
            $form.ajaxSubmit(options);
        }
    }
});

$.extend($.validator.messages, {
    required: 'i18n' in window ? i18n.m1000 : "This field is required!",
    remote: 'i18n' in window ? i18n.m1001 : "Please fix this field.",
    email: 'i18n' in window ? i18n.m1002 : "Please enter a valid email address.",
    url: 'i18n' in window ? i18n.m1003 : "Please enter a valid URL.",
    date: 'i18n' in window ? i18n.m1004 : "Please enter a valid date.",
    dateISO: 'i18n' in window ? i18n.m1005 : "Please enter a valid date (ISO).",
    number: 'i18n' in window ? i18n.m1006 : "Please enter a valid number.",
    digits: 'i18n' in window ? i18n.m1007 : "Please enter only digits.",
    creditcard: 'i18n' in window ? i18n.m1008 : "Please enter a valid credit card number.",
    equalTo: 'i18n' in window ? i18n.m1009 : "Please enter the same value again.",
    accept: 'i18n' in window ? i18n.m1010 : "Please enter a value with a valid extension.",
    regex: 'i18n' in window ? i18n.m1036 : "Please enter a valid value.",
    maxlength: $.validator.format('i18n' in window ? i18n.m1011 : "Please enter no more than {0} characters."),
    minlength: $.validator.format('i18n' in window ? i18n.m1012 : "Please enter at least {0} characters."),
    rangelength: $.validator.format('i18n' in window ? i18n.m1013 : "Please enter a value between {0} and {1} characters long."),
    range: $.validator.format('i18n' in window ? i18n.m1014 : "Please enter a value between {0} and {1}."),
    max: $.validator.format('i18n' in window ? i18n.m1015 : "Please enter a value less than or equal to {0}."),
    min: $.validator.format('i18n' in window ? i18n.m1016 : "Please enter a value greater than or equal to {0}.")
});

$.validator.addMethod('regex', function(value, element, param) {
    var re = new RegExp(param);
    return this.optional(element) || re.test(value);
}, $.validator.messages.regex);