<?php
//exit("\033[1;31mbold red text\033[0m\n");
define('DS', DIRECTORY_SEPARATOR);
define('ENVIRONMENT', getenv('APPLICATION_ENV') ?: 'development');
//define('ENVIRONMENT', 'production');
define('USE_CACHING', in_array(ENVIRONMENT, ['production', 'stable']));

define('ROOT_PATH', realpath(__DIR__ . '/../'));
define('PUBLIC_PATH', realpath(ROOT_PATH . '/public'));
define('DATA_PATH', ROOT_PATH . '/data');
define('VENDOR_PATH', ROOT_PATH . '/vendor');
define('XHPROF', ENVIRONMENT !== 'development' && extension_loaded('xhprof'));

chdir(dirname(__DIR__));

if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

require 'init_autoloader.php';

Zend\Mvc\Application::init(require 'config/application.config.php')->run();