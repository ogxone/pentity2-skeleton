<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 22.09.15
 * Time: 12:29
 */

namespace Formatter\Formatters\Color;


use Formatter\AbstractFormatter;
use Formatter\Exception\FormatterException;

class CliColorFormatter extends AbstractFormatter implements ColorFormatterInterface
{
    private $_color;
    private $_backgroundColor;

    function _formatLogic()
    {
        $coloredString = "";

        $coloredString .= "\033[" . $this->getColor() . "m";
//        $coloredString .= "\033[" . $this->getBackgroundColor() . "m";
        $coloredString .= $this->getString() . "\033[0m";

        return $coloredString;
    }

    public function getDefaultOptions()
    {
        return [
            'tag' => 'span',
            'default_color' => '0;30',
            'default_background_color' => '1;37',
            'colors' => [
                'black' => '0;30',
                'dark_gray' => '1;30',
                'blue' => '0;34',
                'light_blue' => '1;34',
                'green' => '0;32',
                'light_green' => '1;32',
                'cyan' => '0;36',
                'light_cyan' => '1;36',
                'red' => '0;31',
                'light_red' => '1;31',
                'purple' => '0;35',
                'light_purple' => '1;35',
                'brown' => '0;33',
                'yellow' => '1;33',
                'light_gray' => '0;37',
                'white' => '1;37',
            ],
            'background_colors' => [
                'black' => '40',
                'red' => '41',
                'green' => '42',
                'yellow' => '43',
                'blue' => '44',
                'magenta' => '45',
                'cyan' => '46',
                'light_gray' => '47'
            ]
        ];
    }

    public function setColor($color)
    {
        $colors = $this->getOption('colors');
        if (!$color) {
            $this->_color = $this->getOption('default_color');
        } else if (isset($colors[$color])) {
            $this->_color = $colors[$color];
        } else {
            throw new FormatterException(sprintf('Undefined color %s', $color));
        }
    }

    public function getColor()
    {
        return $this->_color;
    }

    public function setBackgroundColor($color)
    {
        $colors = $this->getOption('background_colors');
        if (!$color) {
            $this->_backgroundColor = $this->getOption('default_background_color');
        } else if (isset($colors[$color])) {
            $this->_backgroundColor = $colors[$color];
        } else {
            throw new FormatterException(sprintf('Undefined bg color %s', $color));
        }
    }

    public function getBackgroundColor()
    {
        return $this->_backgroundColor;
    }
}