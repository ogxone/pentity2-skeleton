<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 22.09.15
 * Time: 12:40
 */

namespace Formatter\Formatters\Color;



interface ColorFormatterInterface
{
    public function setColor($color);
    public function getColor();
    public function setBackgroundColor($color);
    public function getBackgroundColor();

}