<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 22.09.15
 * Time: 12:15
 */

namespace Formatter\Formatters\Color;


use Formatter\AbstractFormatter;
use Utils\Helpers\StaticHtmlHelper;

class HtmlColorFormatter extends AbstractFormatter implements ColorFormatterInterface
{
    private $_color;
    private $_backgroundColor;

    function _formatLogic()
    {
        return StaticHtmlHelper::tag(
            $this->getOption('tag'),
            $this->getString(),
            [
                'style' => sprintf('color:%s;background-color:%s', $this->getColor(), $this->getBackgroundColor())
            ]
        );
    }

    public function getDefaultOptions()
    {
        return [
            'tag' => 'span',
            'color' => 'black',
            'background_color' => 'white',
        ];
    }

    public function setColor($color)
    {
        $this->_color = $color;
    }

    public function getColor()
    {
        return $this->_color;
    }

    public function setBackgroundColor($color)
    {
        $this->_backgroundColor = $color;
    }

    public function getBackgroundColor()
    {
        return $this->_backgroundColor;
    }
}