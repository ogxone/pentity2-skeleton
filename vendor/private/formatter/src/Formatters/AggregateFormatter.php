<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 22.09.15
 * Time: 11:54
 */

namespace Formatter\Formatters;



use Formatter\FormatterInterface;

use Formatter\PrepareParamsTrait;
use Utils\Collection\HashMap;
use Utils\Options\OptionsTrait;

class AggregateFormatter extends HashMap implements FormatterInterface
{
    use OptionsTrait, PrepareParamsTrait;

    public function format()
    {
        $this->_prepareParams(func_get_args());
        $content = $this->getString();
        foreach ($this->getKeys() as $formatter) {
            $content = call_user_func_array([$this->get($formatter), 'format'], array_merge([$content], $this->getParams()));
        }
        return call_user_func_array(
            'sprintf',
            array_merge([$content], $this->_params)
        );
    }
}