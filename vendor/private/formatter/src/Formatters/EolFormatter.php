<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 22.09.15
 * Time: 12:09
 */

namespace Formatter\Formatters;


use Formatter\AbstractFormatter;

class EolFormatter extends AbstractFormatter
{

    function _formatLogic()
    {
        return $this->getString() . $this->getOption('separator');
    }

    public function getDefaultOptions()
    {
        return [
            'separator' => false  === strpos(php_sapi_name(), 'cli') ? '<br/>' : PHP_EOL
        ];
    }
}