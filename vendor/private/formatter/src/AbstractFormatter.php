<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 20.09.15
 * Time: 13:06
 */

namespace Formatter;


use Utils\Options\OptionsTrait;

abstract class AbstractFormatter implements FormatterInterface
{
    use OptionsTrait, PrepareParamsTrait;

    public function __construct(Array $options = [])
    {
        $this->setOptions($this->getDefaultOptions());
        $this->setOptions($options, true);
    }

    /**
     * @return string
     */
    public function format()
    {
        $this->_prepareParams(func_get_args());
        return $this->_formatLogic();
    }

    public function getDefaultOptions()
    {
        return [];
    }

    abstract function _formatLogic();
}