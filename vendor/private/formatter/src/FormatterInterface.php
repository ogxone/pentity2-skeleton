<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 20.09.15
 * Time: 13:04
 */

namespace Formatter;

interface FormatterInterface
{
    public function format();
}