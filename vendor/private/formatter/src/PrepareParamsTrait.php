<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 22.09.15
 * Time: 12:02
 */

namespace Formatter;


use Formatter\Exception\FormatterException;

trait PrepareParamsTrait
{
    protected $_string;
    protected $_params;

    protected function _prepareParams(Array $args)
    {
        if (count($args) < 1) {
            throw new FormatterException(sprintf(
                'At least 1 params needed for %s', __METHOD__));
        }
        $this->_string = array_shift($args);
        $this->_params = $args;
    }

    public function getString()
    {
        return $this->_string;
    }

    public function getParams()
    {
        return $this->_params;
    }
}