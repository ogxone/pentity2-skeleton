<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.06.15
 * Time: 20:04
 */

namespace Uploader;


use Zend\Log\Logger;

class AbstractUploader
{
    /** @var null|Logger*/
    protected $_logger;

    public function __construct(Logger $logger = null)
    {
        $this->_logger = $logger;
    }

    protected function _log($message, $level = Logger::ERR)
    {
        if (null !== $this->_logger) {
            $this->_logger->log($level, $message);
        }
    }

    /**
     * @param $bytes
     * @return string
     */
    public function formatSize($bytes)
    {
        $bytes = (int) $bytes;
        if ($bytes > 1024*1024) {
            $rsize = sprintf('%0.2f%s', $bytes/(1024*1024),  " MB");
        } else if ($bytes > 1024) {
            $rsize = sprintf('%0.2f%s', $bytes/1024, " KB");
        } else {
            $rsize = sprintf('%0.2f%s', $bytes, " Bytes");
        }
        return $rsize;
    }
}