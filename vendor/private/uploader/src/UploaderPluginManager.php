<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.06.15
 * Time: 20:05
 */

namespace Uploader;


use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\Exception;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Config\ConfigInterface;

class UploaderPluginManager extends AbstractPluginManager
{
    public function __construct(ConfigInterface $configuration = null)
    {
        parent::__construct($configuration);
        $this->setFactory('file', [$this, '_fileUploaderFactory']);
        $this->setFactory('s3', [$this, '_s3UploaderFactory']);
    }

    /**
     * Validate the plugin
     *
     * Checks that the filter loaded is either a valid callback or an instance
     * of FilterInterface.
     *
     * @param  mixed $plugin
     * @return void
     * @throws Exception\RuntimeException if invalid
     */
    public function validatePlugin($plugin)
    {
        if ($plugin instanceof UploaderInterface){
            return;
        }

        throw new \RuntimeException(sprintf(
            'Plugin of type %s is invalid; must implement Pentity2\Infrastructure\Uploader\UploaderInterface',
            (is_object($plugin) ? get_class($plugin) : gettype($plugin))
        ));
    }

    private function _fileUploaderFactory(ServiceLocatorInterface $sl)
    {

    }

    private function _s3UploaderFactory(ServiceLocatorInterface $sl)
    {

    }
}