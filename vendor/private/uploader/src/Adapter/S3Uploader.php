<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.06.15
 * Time: 19:50
 */

namespace Uploader\Adapter;

use Aws\S3\Enum\CannedAcl;
use Aws\S3\S3Client;
use Zend\Log\Logger;

class S3Uploader extends AbstractUploader
{
    const IMAGE_SECTION = '';

    /** @var null|array  */
    protected $_awsConfig;

    /** @var null|\Aws\S3\S3Client*/
    protected $_awsS3Client;

    public function __construct(S3Client $awsS3Client, Array $config, Logger $logger = null)
    {
        $this->_awsS3Client = $awsS3Client;
        $this->_awsConfig = $config;
        parent::__construct($logger);
    }



    /**
     * @param string $filename      - path to file
     * @param string $size          - type of size of image
     * @param null|string $version  - file version
     * @return string
     */
    public function getImageUrl($filename, $size, $version = null)
    {
        if (empty($version)) {
            $version = $this->getVersion();
        }
        $arguments['VersionId'] = $version;
        $bucket = $this->getBucketName();

        if (!empty($filename)) {
            $url = $this->_awsS3Client
                ->getObjectUrl($bucket, $this->getKey($filename, $size), null, $arguments);
        } else {
            $url = $this->_awsS3Client
                ->getObjectUrl($bucket, $this->getDefaultKey($size), null, $arguments);
        }

        return $url;
    }

    /**
     * @return null
     */
    public function getVersion()
    {
        return null;
    }

    /**
     * Upload file to Amazon Storage
     *
     * @param string $file
     * @param string $key - file key
     * @param bool $isSource
     * @internal param $filename - path to file
     * @return null|\Guzzle\Service\Resource\Model
     */
    public function uploadFile($file, $key, $isSource = false)
    {
        try {
            if (!$isSource) {
                $fContent = file_get_contents ($file);
                if (empty($fContent)) {
                    $error = error_get_last();
                    throw new \Exception($error['message'], $error['type']);
                }
            } else {
                $fContent = $file;
            }
            $fInfo = finfo_open(FILEINFO_MIME_TYPE);
            $fContentType = finfo_buffer($fInfo, $fContent);
            finfo_close($fInfo);

        } catch(\Exception $e) {
            $this->_log($e->getMessage());
            return null;
        }

        try {
            $response = $this->getAwsS3Client()
                ->putObject([
                    'Bucket'        => $this->getBucketName(),
                    'Key'           => $key,
                    'Body'          => $fContent,
                    'ContentType'   => $fContentType,
//                    'SourceFile'    => $filename,
                    'ACL'           => CannedAcl::PUBLIC_READ,
                ]);
            $this->getServiceLocator()
                ->get('Logger')
                ->debug($response);
        } catch (\Exception $e) {
            $this->_log("There was an error uploading the file to Storage.\n");
            return null;
        }
        return $response;
    }

    /**
     * @param string $filename  - path to file
     * @param string $size      - type of size of image
     * @return string
     * @throws \Exception
     */
    public function getKey($filename, $size)
    {
        if (empty($filename)) {
            throw new \Exception("Please set valid not empty file key.");
        }
        $key = sprintf("%s/%s/%s", static::IMAGE_SECTION, $size, urlencode($filename));

        $folder = $this->_awsConfig['folder'];
        if (!empty($folder)) {
            $key = $folder . '/' . $key;
        }

        return $key;
    }

    /**
     * @param string $size - type of size of image
     * @return string
     */
    public function getDefaultKey($size)
    {
        return $this->getKey('default.jpg', $size);
    }

    /**
     * @return string
     */
    public function getBucketName()
    {
        return $this->_awsConfig['bucket'];
    }

    /**
     * @param array $rowset         - list of records
     * @param string $size          - type of size of image
     * @param string $field         - field in record with image key
     * @param null|string $toField  - set image url to record field
     * @internal param string $type - image size
     */
    public function prepareImageUrl(&$rowset, $size, $field, $toField = null)
    {
        $toField = empty($toField) ? $field : $toField;
        foreach($rowset as &$record) {
            $record[$toField] = $this->getImageUrl($record[$field], $size);
        }
        return;
    }
}