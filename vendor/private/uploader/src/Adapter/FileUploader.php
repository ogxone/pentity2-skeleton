<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.06.15
 * Time: 20:03
 */

namespace Uploader\Adapter;


use Pentity2\Infrastructure\Utils\Param\Param;

class FileUploader
{
    const FILE_TYPE = '';

    const FIELD_VERSION = 'version';

    const FILES_ORDER = 100000;

    protected static $_defaultGroup = '';

    /**
     * @param null $options
     * @return string
     */
    public static function getType($options = null)
    {
        return static::FILE_TYPE;
    }

    /**
     * @param int $objId
     * @return string
     */
    public static function getGroup($objId = null)
    {
        $group = static::$_defaultGroup;
        if ($objId && $objId > 0) {
            $k = static::FILES_ORDER;
            $group = '';
            do {
                $gr = floor($objId / $k) * $k;
                $group .= $gr . '/';
                $k /= 10;
            } while ($k > 99);
        }
        return $group;
    }

    public function getVersion($objId)
    {
        $select = $this->_db
            ->select()
            ->from(static::NAME)
            ->where([static::PRIMARY_KEY => $objId])
            ->columns([static::FIELD_VERSION]);
        $result = $this->_createResult($select);
        $version = $result['rowset'][0][static::FIELD_VERSION];
        if ($version < 0) {
            $version = 0;
        }
        return $version;
    }

    public function getNextVersion($params)
    {
        return $this->getVersion($params) + 1;
    }

    public function setVersion($params)
    {

        $objId = Param::strict('object_id', $params);
        $version = Param::strict('version', $params);

        return $this->update([static::PRIMARY_KEY => $objId], [static::FIELD_VERSION => $version], static::NAME);
    }

    public function getDefaultVersion()
    {
        return '1';
    }

    /**
     * @param string $value
     * @param null $options
     * @return mixed
     */
    public function getBasename($value, $options = null)
    {
        return $value;
    }

    public function getFilename($baseName, $objId, $options = null)
    {
        $file = $this->getFolder($objId, $options) . $baseName;
        return $file;
    }

    public function getFolder($objId, $options = null)
    {
        return PUBLIC_PATH . '/' . static::getType($options) . '/' . static::getGroup($objId);
    }

    public function getUrl($baseName, $objId, $options = null)
    {
        $Url = '/' . (static::getType($options) ? static::getType($options) . '/' : '')
            . static::getGroup($objId);
        $filename = $this->getFilename($baseName, $objId, $options);
        $fileUrl = $Url . $baseName;
        if (file_exists($filename)) {
            return $fileUrl;
        } else {
            return false;
        }
    }

    /**
     * @param array $objId
     * @param array $options
     * @return bool
     */
    public function remove($objId, $options = [])
    {
        $baseName = Param::strict('baseName', $options);
        unset($options['baseName']);

        $this->beginTransaction();
        $removed = parent::remove($objId, $options);

        $filename = $this->getFilename($baseName, $objId, $options);
        if ($removed && !empty($baseName) && file_exists($filename)) {
            $removed = @unlink($filename);
        }

        if($removed) {
            $this->commit();
        } else {
            $this->rollback();
        }

        return $removed;
    }

    public function rename($newBaseName, $baseName, $objId, $options = null)
    {
        $newFilename = $this->getFilename($newBaseName, $objId, $options);
        $fileName = $this->getFilename($baseName, $objId, $options);
        $renamed = @rename($fileName, $newFilename);

        return $renamed;
    }

    public function getUploadPath($objId, $options = null)
    {
        $uploadPath = $this->getFolder($objId, $options);
        if (!is_dir($uploadPath)) {
            mkdir($uploadPath, 0775, true);
        }
        return  $uploadPath;
    }

    /**
     * @param $fileName
     * @return string
     */
    public function getFileSize($fileName)
    {
        return $this->formatSize(filesize($fileName));
    }

    /**
     * @param $objId
     * @param $from
     * @param $fileName
     * @param null $options
     * @return bool
     */
    public function uploadFile($objId, $from, $fileName, $options = null)
    {
        $this->getUploadPath($objId, $options);
        $created = copy($from, $this->getFilename($fileName, $objId, $options));
        return $created;
    }
}