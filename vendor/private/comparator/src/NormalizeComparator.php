<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.05.2015
 * Time: 9:33
 */

namespace Comparator;


use Pentity2\Infrastructure\Utils\StringUtils\StringUtils;

class NormalizeComparator extends AbstractComparator
{
    public function compare($val1, $val2)
    {
        return StringUtils::sanitizeString($val1) == StringUtils::sanitizeString($val2);
    }
}