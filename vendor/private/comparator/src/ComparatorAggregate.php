<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.05.2015
 * Time: 9:48
 */

namespace Comparator;


use Pentity2\Infrastructure\Utils\Collection\Collection;

class ComparatorAggregate extends Collection implements ComparatorInterface
{

    public function __construct()
    {
        parent::__construct('Pentity2\Infrastructure\Comparator\ComparatorInterface');
    }

    public function compare($val1, $val2)
    {
        foreach ($this as $comparator) {
            if ($comparator->compare($val1, $val2)) {
                return true;
            }
        }
        return false;
    }
}