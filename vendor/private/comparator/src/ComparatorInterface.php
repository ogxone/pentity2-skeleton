<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.05.2015
 * Time: 9:31
 */

namespace Comparator;


interface ComparatorInterface
{
    public function compare($val1, $val2);
}