<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.05.2015
 * Time: 9:43
 */

namespace Comparator;


use Pentity2\Infrastructure\Utils\StringUtils\StringUtils;

class FuzzyComparator extends AbstractComparator
{
    private $_threshold;

    public function __construct($validationThreshold)
    {
        $this->_threshold = $validationThreshold;
    }

    public function compare($val1, $val2)
    {
        return StringUtils::similarPhrases($val1, $val2) >= $this->_threshold;
    }
}