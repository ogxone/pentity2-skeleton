<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 23.04.2015
 * Time: 21:39
 */

namespace Build;

use Zend\Mvc\MvcEvent;
use Utils\ArrayUtils\ArrayUtils;

class Module
{
    public function getConfig()
    {
        if (ENVIRONMENT != 'production' || 1) {
            $config = ArrayUtils::merge(
                include __DIR__  . '/config/module.config.php',
                include __DIR__  . '/config/routes.config.php'
            );
        } else {
            $config = include __DIR__  . '/config/module.config.php';
        }
        if (defined('BUILD_CONFIG_PATH') && file_exists(BUILD_CONFIG_PATH)) {
            $config = ArrayUtils::merge($config, include BUILD_CONFIG_PATH);
        }
        $a = $config;
        return $config;
    }

    public function onBootstrap(MvcEvent $e)
    {
//        set_error_handler(function($errno, $errstr, $errfile, $errline){
//            throw new \Exception(sprintf(
//                'PHP Error occurred:' . PHP_EOL .
//                'Error number: %s' . PHP_EOL .
//                'Error message: %s' . PHP_EOL .
//                'error file: %s' . PHP_EOL .
//                'Error line: %s' . PHP_EOL
//                , $errno, $errstr, $errfile, $errline));
//        }, E_ALL);
    }
} 