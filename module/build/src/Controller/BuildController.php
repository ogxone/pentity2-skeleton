<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 28.04.2015
 * Time: 23:57
 */

namespace Build\Controller;


use Pentity2\Infrastructure\Mvc\Controller\AbstractActionController;

class BuildController extends AbstractActionController
{
    public function buildAction()
    {
        $blocksList = $this->params('blocks');
        $blocks = explode(',',$blocksList);
        $buildOptions = [
            'build_blocks' => trim($blocksList) ? array_map('trim', $blocks) : []
        ];
        $builder = $this->getServiceLocator()
            ->get('Build\Factory')
            ->createBuild(ENVIRONMENT, $buildOptions);
        if ($builder) {
            $builder->build();
        }
        return $this->response;
    }

    public function clearAction()
    {
        $blocksList = $this->params('blocks');
        $blocks = explode(', ',$blocksList);
        $buildOptions = [
            'build_blocks' => trim($blocksList) ? array_map('trim', $blocks) : []
        ];
        $builder = $this->getServiceLocator()
            ->get('Build\Factory')
            ->createBuild(ENVIRONMENT, $buildOptions);
        if ($builder) {
            $builder->clear($buildOptions);
        }
        return $this->getResponse();
    }
} 