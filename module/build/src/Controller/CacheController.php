<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 28.04.2015
 * Time: 23:57
 */

namespace Build\Controller;

use Pentity2\Infrastructure\Mvc\Controller\AbstractActionController;
use Zend\Console\Request;

class CacheController extends AbstractActionController
{
    public function ccAction()
    {
        $blocksList = $this->params('blocks');
        $blocks = explode(',',$blocksList);
        $buildOptions = [
            'clear_blocks' => trim($blocksList) ? array_map('trim', $blocks) : []
        ];
        $cleaner = $this->getServiceLocator()
            ->get('Build\CacheCleanerFactory')
            ->createCacheCleaner($buildOptions);
        if ($cleaner) {
            $cleaner->clearCache();
        }
        return $this->getResponse();
    }
} 