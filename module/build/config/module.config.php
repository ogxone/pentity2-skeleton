<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 23.04.2015
 * Time: 21:38
 */
return [
    'build' => [
        'common' => [
            'test' => ['name' => 'unittest', 'seq' => 1],
            'composer' => ['name' => 'composer', 'seq' => 1],
            'acl' => ['name' => 'acl', 'seq' => 1],
            'langs' => ['name' => 'langs', 'seq' => 1],
            'templatemap' => ['name' => 'templatemap', 'seq' => 0],
        ],
        'production' => [
            'assets' => ['name' => 'mergeAssets', 'seq' => 3],
            'langs' => ['name' => 'langsVersion', 'seq' => 2],
            'S3' => ['name' => 's3Synchronizer', 'seq' => 1],
        ],
        'development' => [
            'assets' => ['name' => 'plainAssets', 'seq' => 3],
            'langs' => ['name' => 'langs', 'seq' => 2],
        ]
    ],
    'cache_cleaner' => [
        'config' => ['name' => 'file', 'params' => ['Cache\Config', [DATA_PATH . '/cache/config']]],
        'routes' => ['name' => 'tag', 'params' => ['Cache\Routes', [\Index\Application\Service\RouteCache\RouteCacheService::ROUTE_CACHE_TAG]]],
        'pages' => ['name' => 'tag', 'params' => ['Cache\Pages', [\Index\Application\Service\PageCache\PageCacheService::PAGE_CACHE_TAG]]],
        'db_master' => ['name' => 'item', 'params' => ['Cache\Db', [\Pentity2\Infrastructure\Cache\MasterSlaveCache::MASTER_CACHE_ID]]],
        'db_flush' => ['name' => 'flush', 'default' => false, 'params' => ['Cache\Db']],
        'acl' => ['name' => 'item', 'params' => ['Cache\Acl', [\Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl\CachingAcl::ACL_CACHE_KEY]]],
        'translator' => ['name' => 'namespace', 'params' => ['Cache\Translator', 'Cache\Translator']]
    ],
    'controllers' => [
        'invokables' => [
            'Build\Controller\Build' => 'Build\Controller\BuildController' ,
            'Build\Controller\Cache' => 'Build\Controller\CacheController'
        ]
    ],
    'console' => [
        'router' => [
            'routes' => [
                'build' => [
                    'options' => [
                        'route'    => 'build [<blocks>]',
                        'defaults' => [
                            'controller' => 'Build\Controller\Build',
                            'action'     => 'build'
                        ]
                    ]
                ],
                'clear' => [
                    'options' => [
                        'route'    => 'clear [<blocks>]',
                        'defaults' => [
                            'controller' => 'Build\Controller\Build',
                            'action'     => 'clear'
                        ]
                    ]
                ],
                'cc' => [
                    'options' => [
                        'route'    => 'cc [<blocks>]',
                        'defaults' => [
                            'controller' => 'Build\Controller\Cache',
                            'action'     => 'cc'
                        ]
                    ]
                ]
            ]
        ]
    ],
    'log' => [
        'Log\BuildLogger' => [
            'writers' => [
                [
                    'name' => 'stream',
                    'priority' => 1000,
                    'options' => [
                        'stream' => 'php://output'
                    ]
                ]
            ]
        ],
        'Log\BuildErrorLogger' => [
            'writers' => [
                [
                    'name' => 'stream',
                    'priority' => 1001,
                    'options' => [
                        'stream' => strpos(php_sapi_name(), 'cli') ? 'php://stderr' : 'php://output'
                    ]
                ]
            ]
        ]
    ],
    'template_map_exclude_modules' => [
        'Build',
    ]
];