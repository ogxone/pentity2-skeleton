<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.05.2015
 * Time: 14:20
 */
return [
    'router' => [
        'routes' => [
            'http_build' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/build[/:blocks]',
                    'defaults' => [
                        'controller' => 'Build\Controller\Build',
                        'action'     => 'build'
                    ]
                ]
            ],
            'http_clear' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/cc[/:blocks]',
                    'defaults' => [
                        'controller' => 'Build\Controller\Cache',
                        'action'     => 'cc'
                    ]
                ]
            ]
        ]
    ],
];