<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.08.15
 * Time: 18:16
 */

namespace Common;


use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}