<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 06.04.2015
 * Time: 22:50
 */

namespace Common\Domain\Entity;

use Pentity2\Domain\Entity\AbstractEntity;

class UserEntity extends AbstractEntity
{
    const SOCIAL_FACEBOOK = 1;

    protected $_idFieldName = 'user_id';
    protected $_entityFields = [
        'user_id',
        'social_od',
        'social_network',
        'password',
        'username',
        'firstname',
        'lastname',
        'gender',
        'role',
        'email',
        'activity',
    ];
}