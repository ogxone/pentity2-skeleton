<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 23.04.2015
 * Time: 16:31
 */

namespace Common\Domain\Entity;

use Pentity2\Domain\Entity\AbstractEntity;

class LangEntity extends AbstractEntity
{
    protected $_idFieldName = 'lang_id';
    protected $_entityFields = [
        'lang_id',
        'locale',
        'section',
        'code',
        'phrase'
    ];
}
