<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 14:08
 */

namespace Common\Domain\Service\Builder;


use Pentity2\Domain\Service\Builder\AbstractAssemblerManager;
use Zend\ServiceManager\ConfigInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class AssemblerManager extends AbstractAssemblerManager implements ServiceLocatorAwareInterface
{
    public function __construct(ConfigInterface $configuration = null)
    {
        parent::__construct($configuration);
        $this->setFactory('browse', 'Common\Domain\Service\Builder\Assembler\Factory\BrowseAssemblerFactory');
        $this->setFactory('detail', 'Common\Domain\Service\Builder\Assembler\Factory\DetailAssemblerFactory');
    }
}