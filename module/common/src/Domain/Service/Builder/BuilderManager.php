<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 14:08
 */

namespace Common\Domain\Service\Builder;



use Pentity2\Domain\Service\Builder\AbstractBuilderManager;
use Zend\ServiceManager\ConfigInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class BuilderManager extends AbstractBuilderManager implements ServiceLocatorAwareInterface
{
    public function __construct(ConfigInterface $configuration = null)
    {
        parent::__construct($configuration);
        $this->setFactory('article', 'Common\Domain\Service\Builder\Builder\Factory\ArticleBuilderFactory');
        $this->setFactory('video', 'Common\Domain\Service\Builder\Builder\Factory\VideoBuilderFactory');
        $this->setFactory('photo', 'Common\Domain\Service\Builder\Builder\Factory\PhotoBuilderFactory');
    }
}