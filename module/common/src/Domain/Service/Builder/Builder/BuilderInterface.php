<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 08.09.15
 * Time: 20:21
 */

namespace Common\Domain\Service\Builder\Builder;


use \Pentity2\Domain\Service\Builder\Builder\BuilderInterface as BaseBuilder;

interface BuilderInterface extends BaseBuilder
{
    public function buildDetail();
    public function buildBrowse();
    public function buildRelated();
    public function buildPrevNext();
    public function buildLinks();
    public function buildMainPhoto();
}