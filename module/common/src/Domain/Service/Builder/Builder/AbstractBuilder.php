<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 08.09.15
 * Time: 20:39
 */

namespace Common\Domain\Service\Builder\Builder;


use Pentity2\Domain\Repo\Factory\RepoFactoryInterface;

abstract class AbstractBuilder implements BuilderInterface
{
    /**
     * @var RepoFactoryInterface
     */
    protected $_repoFactory;
    protected $_buildable;

    public function __construct(RepoFactoryInterface $factory)
    {
        $this->_repoFactory = $factory;
    }

    public function getResult()
    {
        return $this->_buildable;
    }
}