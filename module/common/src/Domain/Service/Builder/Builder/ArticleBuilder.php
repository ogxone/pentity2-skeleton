<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 08.09.15
 * Time: 20:38
 */

namespace Common\Domain\Service\Builder\Builder;


class ArticleBuilder extends AbstractBuilder
{

    public function buildDetail()
    {

        $this->_buildable = [__METHOD__];
    }

    public function buildBrowse()
    {
        $this->_buildable[] = [__METHOD__];
    }

    public function buildRelated()
    {
        $this->_buildable[] = [__METHOD__];
    }

    public function buildPrevNext()
    {
        $this->_buildable[] = [__METHOD__];
    }

    public function buildLinks()
    {
        $this->_buildable[] = [__METHOD__];
    }

    public function buildMainPhoto()
    {
        $this->_buildable[] = [__METHOD__];
    }
}