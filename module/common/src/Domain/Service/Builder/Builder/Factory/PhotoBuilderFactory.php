<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 14:27
 */

namespace Common\Domain\Service\Builder\Builder\Factory;


use Common\Domain\Service\Builder\Builder\ArticleBuilder;
use Pentity2\Domain\Service\Builder\Builder\Factory\AbstractBuilderFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class PhotoBuilderFactory extends AbstractBuilderFactory
{
    protected function _createBuilderLogic(ServiceLocatorInterface $serviceLocator)
    {
        $sl = $serviceLocator->getServiceLocator();
        return new ArticleBuilder($sl->get('Repo\FactoryC'));
    }
}