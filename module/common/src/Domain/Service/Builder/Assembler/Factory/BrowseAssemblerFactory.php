<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 14:27
 */

namespace Common\Domain\Service\Builder\Assembler\Factory;


use Common\Domain\Service\Builder\Assembler\BrowseAssembler;
use Pentity2\Domain\Service\Builder\Assembler\Factory\AbstractAssemblerFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class BrowseAssemblerFactory extends AbstractAssemblerFactory
{
    protected function _createAssemblerLogic(ServiceLocatorInterface $serviceLocator)
    {
        $sl = $serviceLocator->getServiceLocator();
        return new BrowseAssembler(
            $this->_getBuilder(),
            $sl->get('Cache\Db')
        );
    }
}