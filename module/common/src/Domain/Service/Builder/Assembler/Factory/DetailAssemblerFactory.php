<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 14:27
 */

namespace Common\Domain\Service\Builder\Assembler\Factory;


use Common\Domain\Service\Builder\Assembler\DetailAssembler;
use Pentity2\Domain\Service\Builder\Assembler\Factory\AbstractAssemblerFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class DetailAssemblerFactory extends AbstractAssemblerFactory
{
    protected function _createAssemblerLogic(ServiceLocatorInterface $serviceLocator)
    {
        $sl = $serviceLocator->getServiceLocator();
        return new DetailAssembler(
            $this->_getBuilder(),
            $sl->get('Cache\Db')
        );
    }
}