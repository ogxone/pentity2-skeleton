<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 08.09.15
 * Time: 20:36
 */

namespace Common\Domain\Service\Builder\Assembler;


use Pentity2\Domain\Service\Builder\Assembler\AbstractAssembler;

class BrowseAssembler extends AbstractAssembler
{
    function _assembleLogic(Array $params = [])
    {
        $this->_builder->buildBrowse();
        $this->_builder->buildLinks();
    }
}
