<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.08.15
 * Time: 19:21
 */

namespace Common\Infrastructure\Repo;


use Pentity2\Domain\Entity\RelEntity;
use Pentity2\Infrastructure\Mapper\Manager\Mappers\SqlMapper;
use Pentity2\Infrastructure\Repo\SqlRepo;
use Common\Domain\Entity\UserEntity;
use Common\Domain\Repo\UserRepoInterface;
use Utils\Param\Param;
use Zend\Db\Sql\Predicate\Predicate;

class UserRepo extends SqlRepo implements UserRepoInterface
{
    public function getEntityPrototype()
    {
        return new UserEntity;
    }

    public function userExists(UserEntity $user)
    {
        /**@var $mapper SqlMapper*/
        $mapper = $this->_getMapper();
        $select = $mapper->getSelect();
        $select->columns([$user->user_id]);
        if (isset($user->username)) {
            $select->where(['username' => $user->username], Predicate::OP_OR);
        }
        if (isset($user->email)) {
            $select->where(['email' => $user->email], Predicate::OP_OR);
        }
        return $mapper->exists($user);
    }

    public function saveTest(UserEntity $user, ImageEntity $image)
    {
        $rel = new RelEntity(['user_id', 'image_id']);
        $rel->user_id = $user->user_id;
        $rel->image_id = $image->image_id;
        $this->saveRelation($rel, 'user_image_rel');
    }
}