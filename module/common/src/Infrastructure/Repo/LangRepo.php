<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.08.15
 * Time: 18:11
 */

namespace Common\Infrastructure\Repo;


use Pentity2\Grid\DataProvider\SqlMapperDataProvider;
use Pentity2\Infrastructure\Repo\SqlRepo;
use Common\Domain\Entity\LangEntity;
use Common\Domain\Repo\LangRepoInterface;
use Utils\Param\Param;

class LangRepo extends SqlRepo implements LangRepoInterface
{

    public function getEntityPrototype()
    {
        return new LangEntity;
    }

    public function fetchAllForGrid(Array $params)
    {
        $mapper = $this->_getMapper();
        $this->_setFields($mapper, $params);
        $this->_applyParams($mapper, $params);

        return new SqlMapperDataProvider($mapper, [
            'page' => Param::get('page', $params, 1),
            'page_size' => Param::get('page_size', $params),
        ]);
    }
}