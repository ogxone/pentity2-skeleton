<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 16:02
 */
return [
    'service_manager' => [
        'invokables' => [
            'Assembler\Manager' => 'Common\Domain\Service\Builder\AssemblerManager',
            'Builder\Manager' => 'Common\Domain\Service\Builder\BuilderManager'
        ]
    ]
];