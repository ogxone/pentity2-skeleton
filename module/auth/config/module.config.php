<?php
return [
    'controllers' => [
        'invokables' => [
            'Auth\Controller\Index'           => 'Auth\Controller\IndexController',
            'Auth\Controller\RestorePass'     => 'Auth\Controller\RestorePassController',
            'Auth\Controller\Change'          => 'Auth\Controller\ChangeController',
        ]
    ],

    'service_manager' => [
        'factories' => [
            'Auth\Plain' => 'Auth\Application\Service\Auth\Factory\AuthFactory',
            'Auth\Captcha' => 'Auth\Application\Service\Captcha\Factory\CaptchaFactory',
            'Auth\Register' => 'Auth\Application\Service\Register\Factory\RegisterFactory',
            'Auth\PasswordGenerator' => 'Auth\Infrastructure\Service\PasswordGenerator\Factory\PasswordGeneratorFactory',
            'Auth\PasswordGenerator\Email' => 'Auth\Infrastructure\Service\PasswordGenerator\Factory\PasswordGeneratorEmailFactory',
            'Auth\Change\Pass' => 'Auth\Application\Service\ChangePass\Factory\ChangePassFactory',
            'Auth\Change\Email' => 'Auth\Application\Service\ChangeEmail\Factory\ChangeEmailFactory',
            'Auth\Restore\Pass' => 'Auth\Application\Service\Restore\Factory\RestorePassFactory'
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'router' => [
        'routes' => [
            'register' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/register',
                    'defaults' => [
                        'controller' => 'Auth\Controller\Index',
                        'action' => 'register'
                    ]
                ],
            ],
            'auth' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/auth',
                    'defaults' => [
                        'controller' => 'Auth\Controller\Index',
                        'action' => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'login' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/login',
                            'defaults' => [
                                'controller' => 'Auth\Controller\Index',
                                'action' => 'index'
                            ]
                        ]
                    ],
                    'send-restore-link' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/send-restore-pass-link[/:email]',
                            'defaults' => [
                                'controller' => 'Auth\Controller\RestorePass',
                                'action' => 'send-restore-pass-link'
                            ]
                        ]
                    ],
                    'check-restore-link' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/check-restore-pass-link/:hash/:date/:uid',
                            'defaults' => [
                                'controller' => 'Auth\Controller\RestorePass',
                                'action' => 'check-restore-pass-link'
                            ]
                        ]
                    ],
                    'process-restore-link' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/process-restore-pass-link/:hash/:date/:uid',
                            'defaults' => [
                                'controller' => 'Auth\Controller\RestorePass',
                                'action' => 'process-restore-pass-link'
                            ]
                        ]
                    ],
                    'change-pass' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/change-pass',
                            'defaults' => [
                                'controller' => 'Auth\Controller\Change',
                                'action' => 'change-pass'
                            ]
                        ]
                    ],
                    'change-email' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/change-email',
                            'defaults' => [
                                'controller' => 'Auth\Controller\Change',
                                'action' => 'change-email'
                            ]
                        ]
                    ],
                    'change-email-process' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/change-email-process/:hash/:date/:email',
                            'defaults' => [
                                'controller' => 'Auth\Controller\Change',
                                'action' => 'confirm-change-email'
                            ]
                        ]
                    ],
                    'generate-captcha' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/generate-captcha/[:id]',
                            'defaults' => [
                                'controller' => 'Auth\Controller\Index',
                                'action' => 'generate-captcha'
                            ]
                        ]
                    ]
                ],
            ],
            'logout' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/logout',
                    'defaults' => [
                        'controller' => 'Auth\Controller\Index',
                        'action' => 'logout'
                    ]
                ],
            ],
        ]
    ],
];