<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 03.05.2015
 * Time: 22:12
 */

namespace Auth;


use Pentity2\Infrastructure\Mvc\Module\AbstractModule;
//use Facebook\FacebookSession;
use Zend\Mvc\MvcEvent;

class Module extends AbstractModule
{
    public function getConfig()
    {
        return $this->_mergeTemplateMapToConfig(
            include __DIR__ . '/config/module.config.php',
            __DIR__ . '/template_map.php'
        );
    }

    public function onBootstrap(MvcEvent $e)
    {
        $this->_initFacebookConnection($e);
    }

    private function _initFacebookConnection(MvcEvent $e)
    {
//        $config = $e->getApplication()
//            ->getServiceManager()
//            ->get('Config')['auth']['facebook'];
//        FacebookSession::setDefaultApplication($config['app_id'], $config['app_secret']);
    }
} 