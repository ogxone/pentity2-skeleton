<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.09.15
 * Time: 12:45
 */

namespace Auth\Application\Service\ChangeEmail\Factory;


use Auth\Application\Service\ChangeEmail\ChangeEmailService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ChangeEmailFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new ChangeEmailService(
            $serviceLocator->get('App\AuthService')->getIdentity(),
            $serviceLocator->get('Repo\Factory')->createRepo('User'),
            $serviceLocator->get('Mail\Email'),
            $serviceLocator->get('ControllerPluginManager')->get('Url'),
            $serviceLocator->get('Auth\PasswordGenerator\Email'),
            $serviceLocator->get('Config')['mail']
        );
    }
}