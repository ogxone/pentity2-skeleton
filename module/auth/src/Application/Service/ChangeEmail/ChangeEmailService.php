<?php

/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.09.15
 * Time: 10:40
 */
namespace Auth\Application\Service\ChangeEmail;


use Auth\Infrastructure\Service\PasswordGenerator\PasswordGeneratorService;
use Common\Infrastructure\Repo\UserRepo;
use Mail\Infrastructure\Service\Mail\Email as EmailService;
use Utils\ErrorHandler\StaticErrorHandler;
use Zend\Mvc\Controller\Plugin\Url as UrlPlugin;
use Common\Domain\Entity\UserEntity;
use Pentity2\Application\Service\AbstractService;
use Pentity2\Application\Service\Exception\ServiceException;
use Utils\Param\Param;

class ChangeEmailService extends AbstractService
{
    protected $_me;
    protected $_userRepo;
    protected $_emailService;
    protected $_urlPlugin;
    protected $_passwordGenerator;
    protected $_mailConfig;

    public function __construct(
        UserEntity $me,
        UserRepo $userRepo,
        EmailService $emailService,
        UrlPlugin $urlPlugin,
        PasswordGeneratorService $passwordGenerator,
        Array $mailConfig
    )
    {
        $this->_me = $me;
        $this->_userRepo = $userRepo;
        $this->_emailService = $emailService;
        $this->_urlPlugin = $urlPlugin;
        $this->_passwordGenerator = $passwordGenerator;
        $this->_mailConfig = $mailConfig;
    }

    protected function _executeLogic(Array $params = [])
    {
        $action = Param::get('action', $params);
        switch ($action) {
            case 'sendLink':
                $email = Param::getEmail('email', $params);
                $this->_sendLink($email);
                break;
            case 'processLink':
                $hash = Param::get('hash', $params);
                $date = Param::get('date', $params);
                $email = Param::getEmail('email', $params);
                $this->_processLink($hash, $date, $email);
                break;
            default:
                throw new ServiceException('unknown_action');
        }
    }

    private function _sendLink($email)
    {
        if ($email == $this->_me->email) {
            throw new ServiceException('you_must_input_different_email');
        }
        if ($this->_userRepo->userExists(new UserEntity(['email' => $email]))) {
            throw new ServiceException('user_already_exists');
        }
        $this->_emailService->send('mail/login/change_email', [
            'url' => $this->_createChangeUrl($email),
            'to' => $email,
            'from' => $this->_mailConfig['service_email'],
            'subject' => 'change_email'
        ]);
    }

    private function _processLink($hash, $date, $email)
    {
        $validateHash = $this->_passwordGenerator->create($email . $date);
        if ($hash !== $validateHash) {
            throw new ServiceException('invalid_change_email_link');
        }
        $expirationDate = $date + $this->_mailConfig['activation_link_lifetime'];
        if ($expirationDate < time()) {
            throw new ServiceException('activation_link_expired');
        }
        $result = $this->_userRepo->save($user = new UserEntity(['user_id' => $this->_me->user_id, 'email' => $email]));
        if (false === $result) {
            throw new ServiceException(StaticErrorHandler::getFirstError($user->createIdentifier()));
        }
    }

    private function _createChangeUrl($email)
    {
        $date = time();
        return $this->_urlPlugin->fromRoute('auth/change-email-process', [
            'hash' => $this->_passwordGenerator->create($email . $date),
            'date' => $date,
            'email' => $email
        ], [
           'force_canonical' => true
        ]);
    }
}