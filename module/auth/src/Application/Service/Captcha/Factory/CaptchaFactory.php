<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.09.15
 * Time: 15:38
 */

namespace Auth\Application\Service\Captcha\Factory;


use Zend\Captcha\Factory;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CaptchaFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config')['form']['captcha'];
        $config['options']['imgUrl'] = $serviceLocator->get('ControllerPluginManager')->get('Url')->fromRoute('auth/generate-captcha');
        $captcha = Factory::factory($config);
        return $captcha;
    }
}