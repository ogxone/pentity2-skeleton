<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 28.08.15
 * Time: 20:14
 */

namespace Auth\Application\Service\Register;


use Auth\Application\Service\Auth\AuthService;
use Auth\Infrastructure\Service\PasswordGenerator\PasswordGeneratorService;
use Common\Domain\Entity\UserEntity;
use Common\Infrastructure\Repo\UserRepo;
use Pentity2\Application\Service\AbstractService;
use Pentity2\Domain\Service\Exception\ServiceException;
use Pentity2\Infrastructure\Service\TransactionManager\TransactionManager;
use Utils\ErrorHandler\StaticErrorHandler;
use Utils\Param\Param;

class RegisterService extends AbstractService
{
    private $_userRepo;
    private $_authService;
    private $_transactionManager;
    private $_passwordGenerator;

    public function __construct(
        UserRepo $userRepo,
        AuthService $authService,
        TransactionManager $transactionManager,
        PasswordGeneratorService $passwordGenerator
    )
    {
        $this->_userRepo = $userRepo;
        $this->_authService = $authService;
        $this->_transactionManager = $transactionManager;
        $this->_passwordGenerator = $passwordGenerator;
    }
    protected function _executeLogic(Array $params = [])
    {
        $username = Param::get('username', $params);
        $password = Param::get('password', $params);
        $email = Param::getEmail('email', $params);

        $newUser = new UserEntity([
            'username' => $username,
            'password' => $this->_passwordGenerator->create($password),
            'email' => $email,
            'activity' => 1,
            'role' => 'member'
        ]);
        $this->_transactionManager->beginTransaction();
        if ($this->_userRepo->userExists($newUser)) {
            throw new ServiceException('user_already_exists');
        }
        if (false === $this->_userRepo->save($newUser)) {
            throw new ServiceException(StaticErrorHandler::getFirstError($newUser->createIdentifier()));
        }
        if (false === $this->_authService->execute(['username' => $username, 'password' => $password])) {
            throw new ServiceException(StaticErrorHandler::getFirstError($this->_authService->createIdentifier()));
        }
        $this->_transactionManager->commit();
    }

    protected function _errorLogic()
    {
        $this->_transactionManager->rollback();
    }
}