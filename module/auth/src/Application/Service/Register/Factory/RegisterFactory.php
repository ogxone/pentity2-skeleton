<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 28.08.15
 * Time: 20:28
 */

namespace Auth\Application\Service\Register\Factory;


use Auth\Application\Service\Register\RegisterService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RegisterFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new RegisterService(
            $serviceLocator->get('Repo\Factory')->createRepo('User'),
            $serviceLocator->get('Auth\Plain'),
            $serviceLocator->get('Db\TransactionManager'),
            $serviceLocator->get('Auth\PasswordGenerator')
        );
    }
}