<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 02.09.15
 * Time: 19:48
 */

namespace Auth\Application\Service\ChangePass;


use Auth\Infrastructure\Service\PasswordGenerator\PasswordGeneratorService;
use Common\Domain\Entity\UserEntity;
use Common\Infrastructure\Repo\UserRepo;
use Pentity2\Application\Service\AbstractService;

use Pentity2\Application\Service\Exception\ServiceException;
use Utils\ErrorHandler\StaticErrorHandler;
use Utils\Param\Param;

class ChangePassService extends AbstractService
{
    private $_userRepo;
    private $_loginIdentity;
    private $_passwordGenerator;

    public function __construct(
        UserRepo $repo,
        UserEntity $loginIdentity,
        PasswordGeneratorService $passwordGenerator
    )
    {
        $this->_userRepo = $repo;
        $this->_loginIdentity = $loginIdentity;
        $this->_passwordGenerator = $passwordGenerator;
    }

    protected function _executeLogic(Array $params = [])
    {
        if (null === $this->_loginIdentity) {
            throw new ServiceException('login_identity_not_found');
        }
        $newPassword = Param::strict('password', $params);
        $saved = $this->_userRepo->save($user = new UserEntity([
            'user_id' => $this->_loginIdentity->user_id,
            'password' => $this->_passwordGenerator->create($newPassword)
        ]));
        if (!$saved) {
            throw new ServiceException(StaticErrorHandler::getFirstError($user->createIdentifier()));
        }
    }
}