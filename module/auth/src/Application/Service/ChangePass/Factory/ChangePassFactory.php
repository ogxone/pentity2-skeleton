<?php

/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 02.09.15
 * Time: 19:53
 */
namespace Auth\Application\Service\ChangePass\Factory;


use Auth\Application\Service\ChangePass\ChangePassService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ChangePassFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new ChangePassService(
            $serviceLocator->get('repoFactoryC')->createRepo('User'),
            $serviceLocator->get('App\AuthService')->getIdentity(),
            $serviceLocator->get('Auth\PasswordGenerator')
        );
    }
}