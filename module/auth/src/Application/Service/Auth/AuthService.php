<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 07.05.2015
 * Time: 1:11
 */

namespace Auth\Application\Service\Auth;



use Auth\Infrastructure\Service\PasswordGenerator\PasswordGeneratorService;
use Common\Domain\Entity\UserEntity;
use Pentity2\Application\Service\AbstractService;
use Pentity2\Application\Service\Exception\ServiceException;
use Utils\Param\Param;
use Zend\Authentication\AuthenticationService as ZendAuthService;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter;
use Zend\Authentication\Result;
use Zend\I18n\Translator\Translator;
use Zend\Session\AbstractContainer;
use Zend\Session\Container;

class AuthService extends AbstractService
{
    const LOGIN_FAILURE = 'login_failure';
    /**
     * @var $_zendAutjService ZendAuthService
     */
    private $_zendAuthService;
    /**
     * @var $_passGenerator PasswordGeneratorService
     */
    private $_passGenerator;
    /**
     * @var $_translator Translator
     */
    private $_translator;
    private $_rememberMeLifeTime;
    private $_invalidAuthCountThreshold = 3;
    private $_session;

    public function __construct(
        ZendAuthService $zendAuthService,
        PasswordGeneratorService $passGenerator,
        Translator $translator,
        $rememberMeLifeTime
    )
    {
        $this->_zendAuthService = $zendAuthService;
        $this->_passGenerator=$passGenerator;
        $this->_translator = $translator;
        $this->_session = new Container('Auth\Login');
        if (!isset($this->_session['auth_count'])) {
            $this->_session['auth_count'] = 0;
        }
    }

    protected function _executeLogic(Array $params = [])
    {
        $userName = Param::get('username', $params);
        $password = Param::get('password', $params);
        $rememberMe = Param::get('rememberMe', $params);

        $this->_session['auth_count']++;
        /**@var $adapter CredentialTreatmentAdapter*/
        $adapter = $this->_zendAuthService
            ->getAdapter();
        $adapter->setIdentity($userName)
            ->setCredential($this->_passGenerator->create($password))
            ->setCredentialTreatment('(?) and activity = 1');

        $result = $this->_zendAuthService->authenticate();
        /**@var $result Result*/
        if ($result->isValid()) {
            $userInfo = $this->_zendAuthService
                ->getAdapter()
                ->getResultRowObject(null, 'password');
            $this->_zendAuthService
                ->getStorage()
                ->write(new UserEntity((array)$userInfo));
            if ($rememberMe) {
                $sessionManager = AbstractContainer::getDefaultManager();
                $sessionManager->rememberMe($this->_rememberMeLifeTime);
            }
            $this->_session['auth_count'] = 0;
        } else {
            throw new ServiceException($this->_formatErrorMessage($result->getCode()));
        }
    }

    public function showCaptcha()
    {
        return $this->_session['auth_count'] >= $this->_invalidAuthCountThreshold;
    }

    protected function _formatErrorMessage($code)
    {
        switch ($code) {
            case Result::FAILURE_UNCATEGORIZED:
            case Result::FAILURE_CREDENTIAL_INVALID:
                return $this->_translator->translate('login_credential_invalid');
            case Result::FAILURE_IDENTITY_AMBIGUOUS:
                return $this->_translator->translate('login_identity_ambiguous');
            case Result::FAILURE_IDENTITY_NOT_FOUND:
                return $this->_translator->translate('login_identity_not_found');
            case Result::FAILURE:
                return $this->_translator->translate('login_failure');
        }
        return $this->_translator->translate('login_unknown_failure');
    }
} 