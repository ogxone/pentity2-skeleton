<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.05.2015
 * Time: 3:49
 */

namespace Auth\Application\Service\Auth\Factory;


use Auth\Application\Service\Auth\AuthService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AuthFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new AuthService(
            $serviceLocator->get('App\AuthService'),
            $serviceLocator->get('Auth\PasswordGenerator'),
            $serviceLocator->get('App\Translator'),
            $serviceLocator->get('Config')['auth_session_lifetime']
        );
    }
}