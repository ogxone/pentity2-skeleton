<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 01.09.15
 * Time: 12:01
 */

namespace Auth\Application\Service\Restore\Factory;


use Auth\Application\Service\Restore\RestorePassService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RestorePassFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new RestorePassService(
            $serviceLocator->get('Mail\Email'),
            $serviceLocator->get('Repo\Factory')->createRepo('User'),
            $serviceLocator->get('ControllerPluginManager')->get('Url'),
            $serviceLocator->get('Auth\PasswordGenerator\Email'),
            $serviceLocator->get('Config')['mail']
        );
    }
}