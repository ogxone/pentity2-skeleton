<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 01.09.15
 * Time: 11:54
 */

namespace Auth\Application\Service\Restore;


use Auth\Infrastructure\Service\PasswordGenerator\PasswordGeneratorService;
use Common\Domain\Entity\UserEntity;
use Common\Infrastructure\Repo\UserRepo;
use Mail\Infrastructure\Service\Mail\Email as EmailService;
use Pentity2\Application\Service\AbstractService;
use Pentity2\Domain\Service\Exception\ServiceException;
use Utils\ErrorHandler\StaticErrorHandler;
use Utils\Param\Param;
use Zend\Mvc\Controller\Plugin\Url as UrlPlugin;

class RestorePassService extends AbstractService
{
    protected $_emailService;
    protected $_userRepo;
    protected $_urlPlugin;
    protected $_passwordGenerator;
    protected $_mailConfig;

    public function __construct(
        EmailService $emailService,
        UserRepo $userRepo,
        UrlPlugin $urlPlugin,
        PasswordGeneratorService $passwordGenerator,
        Array $mailConfig
    )
    {
        $this->_emailService = $emailService;
        $this->_userRepo = $userRepo;
        $this->_urlPlugin = $urlPlugin;
        $this->_passwordGenerator = $passwordGenerator;
        $this->_mailConfig = $mailConfig;
    }

    protected function _executeLogic(Array $params = [])
    {
        $action = Param::get('action', $params);
        switch ($action) {
            case 'sendLink':
                $email = Param::get('email', $params, 'invalid_email');
                $this->_sendRestorePassLink($email);
                break;
            case 'checkLink':
                $uid = Param::get('uid', $params);
                $date = Param::get('date', $params);
                $hash = Param::get('hash', $params);
                $this->_checkRestorePassLink($uid, $date, $hash);
                break;
            case 'processLink':
                $date = Param::get('date', $params);
                $hash = Param::get('hash', $params);
                $uid = Param::get('uid', $params);
                $password = Param::strict('password', $params);
                $user = $this->_checkRestorePassLink($uid, $date, $hash);
                $this->_processRestorePathLink($user, $password);
                break;
            default:
                throw new ServiceException('undefined_action');
        }
    }

    private function _sendRestorePassLink($email)
    {
        if (null === ($user = $this->_userRepo->fetchOne(['where' => ['email' => $email]]))) {
            throw new ServiceException('user_not_found');
        }
        $this->_emailService->send('mail/login/restore_password', [
            'url' => $this->_createRestoreUrl($user->user_id, $user->user_id),
            'to' => $email,
            'from' => $this->_mailConfig['service_email'],
            'subject' => 'password_restore'
        ]);
    }

    private function _checkRestorePassLink($uid, $date, $hash)
    {
        if (null === ($user = $this->_userRepo->fetchById($uid))) {
            throw new ServiceException('user_not_found');
        }
        $validateHash = $this->_passwordGenerator->create($user->user_id . $date);
        if ($hash !== $validateHash) {
            throw new ServiceException('invalid_restore_pass_link');
        }
        $expirationDate = $date + $this->_mailConfig['activation_link_lifetime'];
        if ($expirationDate < time()) {
            throw new ServiceException('activation_link_expired');
        }
        return $user;
    }

    private function _processRestorePathLink(UserEntity $user, $password)
    {
        $user->password = $this->_passwordGenerator->create($password);
        if (false === $this->_userRepo->save($user)) {
            throw new \RuntimeException(StaticErrorHandler::getFirstError($user->createIdentifier()));
        }
    }

    private function _createRestoreUrl($userId, $userId)
    {
        $date = time();
        $hash = $this->_passwordGenerator->create($userId . time());
        return $this->_urlPlugin->fromRoute('auth/check-restore-link', [
            'uid' => $userId,
            'date' => $date,
            'hash' => $hash
        ], [
            'force_canonical' => true
        ]);
    }
}