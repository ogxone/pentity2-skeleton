<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 02.09.15
 * Time: 11:41
 */

namespace Auth\Controller;


use Pentity2\Infrastructure\Mvc\Controller\AbstractActionController;
use Utils\ErrorHandler\StaticErrorFormatter;
use Utils\ErrorHandler\StaticErrorHandler;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class RestorePassController extends AbstractActionController
{
    public function sendRestorePassLinkAction()
    {
        $form = $this->getFormFactory()->createForm('RestorePassEmail');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $restorePassService = $this->getServiceLocator()->get('Auth\Restore\Pass');
                if (!$restorePassService->execute(['action' => 'sendLink', 'email' => $form->get('email')->getValue()])) {
                    return new JsonModel([
                        'status' => false,
                        'message' => $this->translate(StaticErrorHandler::getFirstError($restorePassService->getIdentifier()))
                    ]);
                }
                return new JsonModel([
                    'status' => true,
                    'url' => $this->url()->fromRoute('auth/login'),
                    'message' => $this->translate('restore_email_sent')
                ]);
            } else {
                return new JsonModel([
                    'status' => false,
                    'message' => $this->translate(StaticErrorFormatter::createErrorFromInputFilter($form->getInputFilter()))
                ]);
            }
        }
        return new ViewModel([
            'form' => $form
        ]);
    }

    public function checkRestorePassLinkAction()
    {
        $params = [
            'hash' => $this->params()->fromRoute('hash'),
            'date' => $this->params()->fromRoute('date'),
            'uid' => $this->params()->fromRoute('uid')
        ];

        $restorePassService = $this->getServiceLocator()->get('Auth\Restore\Pass');
        if (!$restorePassService->execute(['action' => 'checkLink'] + $params)) {
            throw new \Exception($this->translate(StaticErrorHandler::getFirstError($restorePassService->createIdentifier())));
        }

        return new ViewModel([
                'form' => $this->getFormFactory()->createForm('RestorePass')
            ] + $params);
    }

    public function processRestorePassLinkAction()
    {
        $params = [
            'hash' => $this->params()->fromRoute('hash'),
            'date' => $this->params()->fromRoute('date'),
            'uid' => $this->params()->fromRoute('uid')
        ];
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form = $this->getFormFactory()->createForm('RestorePass');
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $restorePassService = $this->getServiceLocator()->get('Auth\Restore\Pass');
                if (!$restorePassService->execute(['action' => 'processLink', 'password' => $form->get('password')->getValue()] + $params)) {
                    return new JsonModel([
                        'status' => false,
                        'url' => $this->url()->fromRoute('auth/check-restore-link', $params),
                        'message' => $this->translate(StaticErrorHandler::getFirstError($restorePassService->getIdentifier()))
                    ]);
                } else {
                    return new JsonModel([
                        'status' => true,
                        'url' => $this->url()->fromRoute('auth/login'),
                        'message' => 'password_changed'
                    ]);
                }
            } else {
                return new JsonModel([
                    'status' => false,
                    'message' => $this->translate(StaticErrorFormatter::createErrorFromInputFilter($form->getInputFilter()))
                ]);
            }
        } else {
            $this->response->setStatusCode(404);
            return $this->response;
        }
    }
}