<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 06.04.2015
 * Time: 23:05
 */

namespace Auth\Controller;

use Common\Domain\Entity\UserEntity;
use Pentity2\Infrastructure\Mvc\Controller\AbstractActionController;
use Utils\ErrorHandler\StaticErrorFormatter;
use Utils\ErrorHandler\StaticErrorHandler;
use Zend\Form\Form;
use Zend\Validator\EmailAddress;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        /**@var $authForm Form*/
        $authService = $this->getServiceLocator()->get('Auth\Plain');
        $authForm = $this->getFormFactory()->createForm('auth', ['show_captcha' => $authService->showCaptcha()]);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $authForm->setData($request->getPost());
            if ($authForm->isValid()) {
                $result = $authService->execute($authForm->getData());
                $updateCaptchaParams = $authService->showCaptcha() ? ['url' => ''] : [];
                if ($result) {
                    return new JsonModel([
                        'status' => true,
                        'url' => $this->url()->fromRoute('home'),
                        'message' => 'Hello ' . $this->identity()->username
                    ]);
                } else {
                    return new JsonModel($updateCaptchaParams + [
                        'status' => false,
                        'message' => $this->translate(StaticErrorHandler::getFirstError($authService->getIdentifier()))
                    ]);
                }
            } else {
                return new JsonModel([
                    'status' => false,
                    'message' => $this->translate(StaticErrorFormatter::createErrorFromInputFilter($authForm->getInputFilter()))
                ]);
            }
        }
        return new ViewModel(['authForm' => $authForm]);
    }

    public function logoutAction()
    {
        $this->getServiceLocator()
            ->get('App\AuthService')
            ->clearIdentity();
        return new JsonModel([
            'status' => true,
            'message' => 'bye'
        ]);
    }

    public function registerAction()
    {
        $form = $this->getFormFactory()->createForm('register');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($form->getInputfilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $regService = $this->getServiceLocator()->get('Auth\Register');
                $result = $regService->execute($form->getData());
                if ($result) {
                    return new JsonModel([
                        'status' => true,
                        'url' => $this->url()->fromRoute('home'),
                        'message' => 'Hello ' . $this->identity()->username
                    ]);
                } else {
                    return new JsonModel([
                        'status' => false,
                        'message' => $this->translate(StaticErrorHandler::getFirstError($regService->getIdentifier()))
                    ]);
                }
            } else {
                return new JsonModel([
                    'status' => false,
                    'message' => $this->translate(StaticErrorFormatter::createErrorFromInputFilter($form->getInputFilter()))
                ]);
            }
        }
        return new ViewModel([
            'regForm' => $form
        ]);
    }

    public function generateCaptchaAction()
    {
        $response = $this->getResponse();
        $response->getHeaders()
            ->addHeaderLine('Content-Type', "image/png");
        $id = $this->params('id', false);
        if ($id) {
            $image = PUBLIC_PATH  . '/data/captcha/' . $id . $this->getServiceLocator()->get('Config')['form']['captcha']['options']['suffix'];
            if (file_exists($image) !== false) {
                $content = file_get_contents($image);
                $response->setStatusCode(200);
                $response->setContent($content);
                if (file_exists($image) == true) {
                    unlink($image);
                }
            }
        }
        return $response;
    }
}