<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 02.09.15
 * Time: 13:31
 */

namespace Auth\Controller;


use Pentity2\Infrastructure\Mvc\Controller\AbstractActionController;
use Utils\ErrorHandler\StaticErrorFormatter;
use Utils\ErrorHandler\StaticErrorHandler;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class ChangeController extends AbstractActionController
{
    public function changePassAction()
    {
        $form = $this->getFormFactory()->createForm('ChangePass');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $changePassService = $this->getServiceLocator()->get('Auth\Change\Pass');
                if ($changePassService->execute(['password' => $form->get('password')->getValue()])) {
                    return new JsonModel([
                        'status' => true,
                        'url' => $this->url()->fromRoute('home'),
                        'message' => $this->translate('password_changed')
                    ]);
                } else {
                    return new JsonModel([
                        'status' => false,
                        'message' => $this->translate(StaticErrorHandler::getFirstError($changePassService->getIdentifier()))
                    ]);
                }
            } else {
                return new JsonModel([
                    'status' => false,
                    'message' => $this->translate(StaticErrorFormatter::createErrorFromInputFilter($form->getInputFilter()))
                ]);
            }
        }
        return new ViewModel([
            'form' => $form
        ]);
    }

    public function changeEmailAction()
    {
        $form = $this->getFormFactory()->createForm('ChangeEmail');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $changePassService = $this->getServiceLocator()->get('Auth\Change\Email');
                if ($changePassService->execute(['action' => 'sendLink', 'email' => $form->get('email')->getValue()])) {
                    return new JsonModel([
                        'status' => true,
                        'url' => $this->url()->fromRoute('home'),
                        'message' => $this->translate('change_email_sent')
                    ]);
                } else {
                    return new JsonModel([
                        'status' => false,
                        'message' => $this->translate(StaticErrorHandler::getFirstError($changePassService->getIdentifier()))
                    ]);
                }
            } else {
                return new JsonModel([
                    'status' => false,
                    'message' => $this->translate(StaticErrorFormatter::createErrorFromInputFilter($form->getInputFilter()))
                ]);
            }
        }
        return new ViewModel([
            'form' => $form
        ]);
    }

    public function confirmChangeEmailAction()
    {
        $params = [
            'hash' => $this->params()->fromRoute('hash'),
            'date' => $this->params()->fromRoute('date'),
            'email' => $this->params()->fromRoute('email')
        ];
        $changePassService = $this->getServiceLocator()->get('Auth\Change\Email');
        if (!$changePassService->execute(['action' => 'processLink'] + $params)) {
            throw new \Exception($this->translate(StaticErrorHandler::getFirstError($changePassService->getIdentifier())));
        }
        return new ViewModel;
    }
}