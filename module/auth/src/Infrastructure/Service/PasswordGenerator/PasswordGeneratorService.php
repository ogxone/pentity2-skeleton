<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 07.05.2015
 * Time: 1:09
 */

namespace Auth\Infrastructure\Service\PasswordGenerator;

use Pentity2\Application\Service\Exception\ServiceException;
use Zend\Crypt\Password\Bcrypt;

class PasswordGeneratorService
{
    private $_salt;
    private $_hashMethod;
    private $_verifyMethod;

    public function __construct($salt, $hashMethod)
    {
        $this->salt = $salt;
        $this->_createHashMethod($hashMethod);
    }

    /**
     * @param $name
     * @throws ServiceException
     */
    private function _createHashMethod($name)
    {
        $hashMethodName = '_hash' . ucfirst(trim($name));
        $verifyMethodName = '_verify' . ucfirst(trim($name));
        if (
            !method_exists($this, $hashMethodName) ||
            !method_exists($this, $verifyMethodName)
        ) {
            throw new ServiceException(
                sprintf('Undefined hash method: %s', $name)
            );
        }
        $this->_hashMethod = $hashMethodName;
        $this->_verifyMethod = $verifyMethodName;
    }

    public function create($password)
    {
        return $this->{$this->_hashMethod}($password);
    }

    public function verify($password, $hash)
    {
        return $this->{$this->_verifyMethod}($password, $hash);
    }

    private function _hashMd5($value)
    {
        return md5($this->_salt . $value);
    }

    private function _hashSha1($value)
    {
        return sha1($this->_salt . $value);
    }

    private function _hashBcrypt($value)
    {
        $bcrypt = new Bcrypt;
        $bcrypt->setCost(14);
        return $bcrypt->create($value);
    }

    private function _verifyMd5($password, $hash)
    {
        return $hash == md5($this->_salt . $password);
    }

    private function _verifySha1($password, $hash)
    {
        return $hash == md5($this->_salt . $password);
    }

    private function _verifyBcrypt($password, $hash)
    {
        $bcrypt = new Bcrypt;
        $bcrypt->setCost(14);
        return $bcrypt->verify($password, $hash);
    }
} 