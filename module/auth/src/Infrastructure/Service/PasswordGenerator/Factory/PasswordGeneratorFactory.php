<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 18.08.15
 * Time: 20:20
 */

namespace Auth\Infrastructure\Service\PasswordGenerator\Factory;


use Auth\Infrastructure\Service\PasswordGenerator\PasswordGeneratorService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PasswordGeneratorFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $salt = $serviceLocator->get('config')['salt'];
        return new PasswordGeneratorService($salt, 'md5');
    }
}