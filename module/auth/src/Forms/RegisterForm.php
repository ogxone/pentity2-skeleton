<?php

namespace Auth\Forms;


use Auth\Forms\Validator\RecordNotExists;
use Pentity2\Infrastructure\Mvc\Forms\AbstractForm;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Session\Container;
use Zend\Validator\Callback;
use Zend\Validator\EmailAddress;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

class RegisterForm extends AbstractForm implements InputFilterAwareInterface
{
    protected $_authCount;
    /**
     * @var  Container
    */
    protected $_session;
    protected $_repo;

    protected function _init()
    {
        $sl = $this->getServiceLocator();
        $captcha = $sl->get('Auth\Captcha');
        $this->_repo = $sl->get('Repo\FactoryC')->createRepo('User');
        $this->add([
            'name' => 'username',
            'type' => 'Text',
            'options' => [
                'label' => 'username',
            ],
        ]);
        $this->add([
            'name' => 'email',
            'type' => 'Text',
            'options' => [
                'label' => 'email',
            ],
        ]);
        $this->add([
            'name' => 'password',
            'type' => 'Password',
            'options' => [
                'label' => 'password',
            ],
        ]);
        $this->add([
            'name' => 'password_confirm',
            'type' => 'Password',
            'options' => [
                'label' => 'password_confirm',
            ],
        ]);
        $this->add([
            'name' => 'captcha',
            'type' => 'Captcha',
            'attributes' => [
                'required'     => 'required'
            ],
            'options' => [
                'label' => 'verify_human',
                'captcha' => $captcha
            ]
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'submit',
                'id' => 'submitbutton',
            ],
        ]);
    }

    /**
     * Retrieve input filter
     *
     * @param InputFilterInterface $inputFilter
     * @return InputFilterInterface
     */
    protected function _configureInputFilter(InputFilterInterface $inputFilter)
    {
        $inputFilter->add([
            'name' => 'username',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'login_too_short'
                    ]
                ],
                [
                    'name' => 'StringLength',
                    'options' => [
                        'messages' => [
                            StringLength::TOO_SHORT => 'login_too_short',
                            StringLength::TOO_LONG => 'login_too_long'
                        ],
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
                [
                    'name' => 'Auth\Forms\Validator\RecordNotExists',
                    'options' => [
                        'repo' => $this->_repo,
                        'field' => 'username',
                        'messages' => [
                            RecordNotExists::RECORD_EXISTS => 'user_already_exists'
                        ]
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'password_too_short'
                    ]
                ],
                [
                    'name' => 'StringLength',
                    'options' => [
                        'messages' => [
                            StringLength::TOO_SHORT => 'password_too_short',
                            StringLength::TOO_LONG => 'password_too_long'
                        ],
                        'encoding' => 'UTF-8',
                        'min' => 4,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'password_confirm',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'password_confirmation_too_short'
                    ]
                ],
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => function($value){
                            return $this->get('password')->getValue() == $value;
                        },
                        'messages' => [
                            Callback::INVALID_VALUE => 'password_confirmation_fail'
                        ]
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'email',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'email_required'
                    ]
                ],
                [
                    'name' => 'Auth\Forms\Validator\RecordNotExists',
                    'options' => [
                        'repo' => $this->_repo,
                        'field' => 'email',
                        'messages' => [
                            RecordNotExists::RECORD_EXISTS => 'email_already_exists'
                        ]
                    ],
                ],
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'messages' => [
                            EmailAddress::INVALID            => "invalid_email",
                            EmailAddress::INVALID_FORMAT     => "invalid_email",
                            EmailAddress::INVALID_HOSTNAME   => "invalid_email",
                            EmailAddress::INVALID_MX_RECORD  => "invalid_email",
                            EmailAddress::INVALID_SEGMENT    => "invalid_email",
                            EmailAddress::DOT_ATOM           => "invalid_email",
                            EmailAddress::QUOTED_STRING      => "invalid_email",
                            EmailAddress::INVALID_LOCAL_PART => "invalid_email",
                            EmailAddress::LENGTH_EXCEEDED    => "invalid_email",
                        ]
                    ]
                ]
            ],
        ]);
    }
}