<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 01.09.15
 * Time: 19:40
 */

namespace Auth\Forms;


use Pentity2\Infrastructure\Mvc\Forms\AbstractForm;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\Callback;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

class RestorePassForm extends AbstractForm implements InputFilterAwareInterface
{
    protected $_inputFilter;

    protected function _init()
    {
        $this->add([
            'name' => 'password',
            'type' => 'password',
            'options' => [
                'label' => 'password'
            ]
        ]);
        $this->add([
            'name' => 'password_confirm',
            'type' => 'password',
            'options' => [
                'label' => 'password_confirm'
            ]
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'submit',
                'id' => 'submitbutton',
            ],
        ]);
    }

    protected function _configureInputFilter(InputFilterInterface $inputFilter)
    {
        $inputFilter->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'password_too_short'
                    ]
                ],
                [
                    'name' => 'StringLength',
                    'options' => [
                        'messages' => [
                            StringLength::TOO_SHORT => 'password_too_short',
                            StringLength::TOO_LONG => 'password_too_long'
                        ],
                        'encoding' => 'UTF-8',
                        'min' => 4,
                        'max' => 100,
                    ],
                ],
            ],
        ]);
        $inputFilter->add([
            'name' => 'password_confirm',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'password_confirmation_too_short'
                    ]
                ],
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => function($value){
                            return $this->get('password')->getValue() == $value;
                        },
                        'messages' => [
                            Callback::INVALID_VALUE => 'password_confirmation_fail'
                        ]
                    ]
                ],
            ],
        ]);
    }
}