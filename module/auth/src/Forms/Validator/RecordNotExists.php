<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.09.15
 * Time: 19:07
 */

namespace Auth\Forms\Validator;


use Pentity2\Domain\Entity\EntityInterface;

class RecordNotExists extends RecordExists
{
    protected function _isValidLogic(EntityInterface $row = null)
    {
        $res = null === $row;
        if (!$res) {
            $this->error(self::RECORD_EXISTS);
        }
        return $res;
    }
}