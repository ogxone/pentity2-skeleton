<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 02.09.15
 * Time: 18:25
 */

namespace Auth\Forms\Validator;


use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Domain\Repo\RepoInterface;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Where;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class RecordExists extends AbstractValidator
{
    const RECORD_NOT_EXISTS = 1;
    const RECORD_EXISTS = 2;

    const TYPE_MATCH = 1;
    const TYPE_NOT_MATCH = 2;

    /**
     * @var $_repo RepoInterface
    */
    private $_repo;
    /**
     * @var $_repo Where
     */
    private $_where = [];
    private $_field;
    protected $messageTemplates = [
        self::RECORD_EXISTS => 'record_exists',
        self::RECORD_NOT_EXISTS => 'record_not_exists'
    ];

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        $this->_validateInput();
        if ($this->_where instanceof Where) {
            $this->_where->addPredicate(new Predicate([$this->_field => $value]));
        } else {
            $this->_where[$this->_field] = $value;
        }
        $res = $this->_repo->fetchOne(['where' => $this->_where]);
        return $this->_isValidLogic($res);
    }

    public function setRepo(RepoInterface $repo)
    {
        $this->_repo = $repo;
    }

    public function setField($field)
    {
        $this->_field = $field;
    }

    public function setWhere($where)
    {
        if ($where instanceof Where) {
            $this->_where = $where;
        } else if (is_array($where)) {
            $this->_where = $where;
        } else {
            throw new Exception\RuntimeException('Where have to be either Zend\Db\Sql\Where or array');
        }
    }

    private function _validateInput()
    {
        if (null === $this->_repo) {
            throw new \RuntimeException('Repo was not provided');
        }
        if (null === $this->_where) {
            throw new Exception\RuntimeException('Either select or where have to be provided');
        }
        if (null === $this->_field) {
            throw new Exception\RuntimeException('Field have to be installed');
        }
    }

    protected function _isValidLogic(EntityInterface $row = null)
    {
        $res = null !== $row;
        if (!$res) {
            $this->error(self::RECORD_NOT_EXISTS);
        }
        return $res;
    }
}