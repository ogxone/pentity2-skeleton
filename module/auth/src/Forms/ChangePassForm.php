<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 01.09.15
 * Time: 19:40
 */

namespace Auth\Forms;


use Auth\Forms\Validator\RecordExists;
use Pentity2\Infrastructure\Mvc\Forms\AbstractForm;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\Callback;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

class ChangePassForm extends AbstractForm implements InputFilterAwareInterface
{
    protected function _init()
    {
        $this->add([
            'name' => 'old_password',
            'type' => 'password',
            'options' => [
                'label' => 'old_password'
            ]
        ]);
        $this->add([
            'name' => 'old_password_hash',
            'type' => 'hidden',
            'options' => [
                'label' => 'old_password_hash'
            ]
        ]);
        $this->add([
            'name' => 'password',
            'type' => 'password',
            'options' => [
                'label' => 'password'
            ]
        ]);
        $this->add([
            'name' => 'password_confirm',
            'type' => 'password',
            'options' => [
                'label' => 'password_confirm'
            ]
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'submit',
                'id' => 'submitbutton',
            ],
        ]);
    }

    public function setData($data)
    {
        parent::setData($data);
        $password = $this->get('old_password')->getValue();
        $passwordHash = $this->getServiceLocator()->get('Auth\PasswordGenerator')->create($password);
        $this->get('old_password_hash')->setValue($passwordHash);
        $this->data['old_password_hash'] = $passwordHash;
        return $this;
    }

    protected function _configureInputFilter(InputFilterInterface $inputFilter)
    {
        $inputFilter->add([
            'name' => 'old_password',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'password_too_short'
                    ]
                ],
                [
                    'name' => 'StringLength',
                    'options' => [
                        'messages' => [
                            StringLength::TOO_SHORT => 'password_too_short',
                            StringLength::TOO_LONG => 'password_too_long'
                        ],
                        'encoding' => 'UTF-8',
                        'min' => 4,
                        'max' => 100,
                    ],
                ],
            ],
        ]);
        $inputFilter->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'password_too_short'
                    ]
                ],
                [
                    'name' => 'StringLength',
                    'options' => [
                        'messages' => [
                            StringLength::TOO_SHORT => 'password_too_short',
                            StringLength::TOO_LONG => 'password_too_long'
                        ],
                        'encoding' => 'UTF-8',
                        'min' => 4,
                        'max' => 100,
                    ],
                ],
            ],
        ]);
        $inputFilter->add([
            'name' => 'password_confirm',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'password_confirmation_too_short'
                    ]
                ],
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => function($value){
                            return $this->get('password')->getValue() == $value;
                        },
                        'messages' => [
                            Callback::INVALID_VALUE => 'password_confirmation_fail'
                        ]
                    ]
                ],
            ],
        ]);
        $inputFilter->add([
            'name' => 'old_password_hash',
            'required' => false,
            'validators' => [
                [
                    'name' => 'Auth\Forms\Validator\RecordExists',
                    'options' => [
                        'repo' => $this->getServiceLocator()->get('repoFactoryC')->createRepo('User'),
                        'field' => 'password',
                        'where' => ['user_id' => $this->getServiceLocator()->get('App\AuthService')->getIdentity()->user_id],
                        'messages' => [
                            RecordExists::RECORD_NOT_EXISTS => 'wrong_password'
                        ]
                    ],
                ],
            ],
        ]);
    }
}