<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.09.15
 * Time: 10:59
 */

namespace Auth\Forms;


use Pentity2\Infrastructure\Mvc\Forms\AbstractForm;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\EmailAddress;
use Zend\Validator\NotEmpty;

class ChangeEmailForm extends AbstractForm implements InputFilterAwareInterface
{
    protected $_inputFilter;

    protected function _init()
    {
        $this->add([
            'name' => 'email',
            'type' => 'Text',
            'options' => [
                'label' => 'new_email',
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'submit',
                'id' => 'submitbutton',
            ],
        ]);
    }

    protected function _configureInputFilter(InputFilterInterface $inputFilter)
    {
        $inputFilter->add([
            'name' => 'email',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'email_required'
                    ]
                ],
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'messages' => [
                            EmailAddress::INVALID            => "invalid_email",
                            EmailAddress::INVALID_FORMAT     => "invalid_email",
                            EmailAddress::INVALID_HOSTNAME   => "invalid_email",
                            EmailAddress::INVALID_MX_RECORD  => "invalid_email",
                            EmailAddress::INVALID_SEGMENT    => "invalid_email",
                            EmailAddress::DOT_ATOM           => "invalid_email",
                            EmailAddress::QUOTED_STRING      => "invalid_email",
                            EmailAddress::INVALID_LOCAL_PART => "invalid_email",
                            EmailAddress::LENGTH_EXCEEDED    => "invalid_email",
                        ]
                    ]
                ]
            ],
        ]);
    }
}