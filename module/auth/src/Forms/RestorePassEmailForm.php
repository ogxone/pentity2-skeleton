<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 03.09.15
 * Time: 10:59
 */

namespace Auth\Forms;


use Pentity2\Infrastructure\Mvc\Forms\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\Validator\EmailAddress;
use Zend\Validator\NotEmpty;

class RestorePassEmailForm extends ChangeEmailForm implements InputFilterAwareInterface
{
    protected function _init()
    {
        parent::_init();
        $this->get('email')->setLabel('input_your_email');
    }
}