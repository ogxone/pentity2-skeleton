<?php

namespace Auth\Forms;

use Pentity2\Infrastructure\Mvc\Forms\AbstractForm;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

class AuthForm extends AbstractForm implements InputFilterAwareInterface
{
    protected function _init()
    {
        $this->add([
            'name' => 'username',
            'type' => 'Text',
            'options' => [
                'label' => 'username',
            ],
        ]);
        $this->add([
            'name' => 'password',
            'type' => 'Password',
            'options' => [
                'label' => 'password',
            ],
        ]);
        $this->add([
            'name' => 'remember',
            'type' => 'checkbox',
            'options' => [
                'label' => 'remember_me',
            ],
        ]);

        if ($this->getOption('show_captcha')) {
            $this->add([
                'name' => 'captcha',
                'type' => 'Captcha',
                'attributes' => [
                    'required'     => 'required'
                ],
                'options' => [
                    'label' => 'verify_human',
                    'captcha' => $this->getServiceLocator()->get('Auth\Captcha'),
                ]
            ]);
        }

        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'submit',
                'id' => 'submitbutton',
            ],
        ]);
    }

    /**
     * Retrieve input filter
     *
     * @param InputFilterInterface $inputFilter
     * @return InputFilterInterface
     */
    protected function _configureInputFilter(InputFilterInterface $inputFilter)
    {
        $inputFilter->add([
            'name' => 'username',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'login_too_short'
                    ]
                ],
                [
                    'name' => 'StringLength',
                    'options' => [
                        'messages' => [
                            StringLength::TOO_SHORT => 'login_too_short',
                            StringLength::TOO_LONG => 'login_too_long'
                        ],
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        NotEmpty::IS_EMPTY => 'password_too_short'
                    ]
                ],
                [
                    'name' => 'StringLength',
                    'options' => [
                        'messages' => [
                            StringLength::TOO_SHORT => 'password_too_short',
                            StringLength::TOO_LONG => 'password_too_long'
                        ],
                        'encoding' => 'UTF-8',
                        'min' => 4,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'remember',
            'required' => false,
            'filters'  => [
                ['name' => 'Int'],
            ],
        ]);
    }
}