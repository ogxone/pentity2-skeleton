<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.05.2015
 * Time: 10:00
 */

namespace Tester\controller;

use Pentity2\Infrastructure\Mvc\Controller\AbstractActionController;



class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $tester = $this->getServiceLocator()->get('Tester\Tester');
        $out = $tester->runTestsAndReportFailures();
        if ($tester->hasError()) {
            echo ($tester->getErrorsAsString());
        } else {
            if (false !== strpos(php_sapi_name(), 'cli')) {
                echo implode(PHP_EOL, $out);
            } else {
                readfile(ROOT_PATH . '/tests/_output/report.html');
            }
        }
        return $this->response;
    }

    public function customAction()
    {

    }
}