<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.05.2015
 * Time: 10:11
 */

namespace Tester\Application\Service\Tester;


use Mail\Factory;
use Utils\ErrorHandler\ErrorHandler;
use Utils\System\Exec;

use Zend\Mail\Headers;
use Zend\View\Model\ViewModel;

class TesterService
{
    use ErrorHandler;

    private $_testsResultFolder;
    private $_mailFactory;
    private $_receivers;

    public function __construct(Factory $mailFactory, Array $receivers)
    {
        $this->_mailFactory = $mailFactory;
        $this->_receivers = $receivers;
        $this->_testsResultFolder  = ROOT_PATH . '/tests/_output';
    }

    public function runTestsAndReportFailures()
    {
        $cmd = Exec::withCommandLine('php codecept.phar run --steps --xml --html')
            ->launch();

        if ($this->_checkTestFailed()) {
            $this->_sendErrorReport();
        }
        return $cmd->getOutput();
    }

    private function _checkTestFailed()
    {
        if (file_exists($report = $this->_testsResultFolder . '/report.xml')) {
            $reportXml = simplexml_load_file($report);
            $errorNodes = $reportXml->xpath('//testsuites//testsuite[@failures > 0]');
            if (!empty($errorNodes)) {
                return true;
            }
        }
        return false;
    }

    private function _sendErrorReport()
    {
        $template = new ViewModel;
        $template->setTemplate('test/report');
        $message = $this->_mailFactory->prepareMessage($template);
        $headers = new Headers;
        $headers->addHeaderLine('Content-type:text/html;charset=utf-8');
        $message->setHeaders($headers);
        $message->setSubject('Car1 test results');
        $message->setTo($this->_receivers);
        $transport = $this->_mailFactory->getTransport();
        $transport->send($message);
    }
}