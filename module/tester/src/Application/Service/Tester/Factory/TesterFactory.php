<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.05.2015
 * Time: 11:19
 */

namespace Tester\Application\Service\Tester\Factory;



use Tester\Application\Service\Tester\TesterService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TesterFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new TesterService(
            $serviceLocator->get('MailFactory'),
            $serviceLocator->get('Config')['tests']['errors_receivers']
        );
    }
}