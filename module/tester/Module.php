<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.05.2015
 * Time: 9:55
 */

namespace Tester;

class Module
{
    public function getConfig()
    {
        if (ENVIRONMENT != 'production') {
            return array_merge_recursive(
                include __DIR__  . '/config/module.config.php',
                include __DIR__  . '/config/routes.config.php'
            );
        }
        return include __DIR__ . '/config/module.config.php';
    }
}