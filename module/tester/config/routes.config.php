<?php
return [
    'controllers' => [
        'invokables' => [
            'Tester\controller\Autoloaddb' => 'Tester\controller\AutoloaddbController',
        ]
    ],
    'router' => [
        'routes' => [
            'custom_test' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/custom_tests',
                    'defaults' => [
                        'controller' => 'Tester\controller\Index',
                        'action'     => 'custom'
                    ]
                ]
            ],
            'run_tests' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/run_tests',
                    'defaults' => [
                        'controller' => 'Tester\controller\Index',
                        'action'     => 'index'
                    ]
                ]
            ],
            'autoload_db' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/autoload_db',
                    'defaults' => [
                        'controller' => 'Tester\controller\Autoloaddb',
                        'action'     => 'index'
                    ]
                ]
            ],
            'grid' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/grid',
                    'defaults' => [
                        'controller' => 'Tester\controller\Grid',
                        'action'     => 'grid'
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'create_db_record' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/create',
                            'defaults' => [
                                'controller' => 'Tester\controller\Grid',
                                'action' => 'create'
                            ]
                        ],
                    ],
                    'update_db_record' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/update/[:id]',
                            'defaults' => [
                                'controller' => 'Tester\controller\Grid',
                                'action' => 'update'
                            ]
                        ],
                    ],
                    'delete_db_record' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/delete/[:id]',
                            'defaults' => [
                                'controller' => 'Tester\controller\Grid',
                                'action' => 'delete'
                            ]
                        ],
                    ],
                ],
            ],

        ]
    ],
];