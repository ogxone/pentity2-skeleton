<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.05.2015
 * Time: 9:57
 */
return [
    'service_manager' => [
        'factories' => [
            'Tester\Tester' => 'Tester\Application\Tester\Factory\TesterFactory'
        ]
    ],
    'controllers' => [
        'invokables' => [
            'Tester\Controller\Index' => 'Tester\Controller\IndexController',
        ]
    ],
    'console' => [
        'router' => [
            'routes' => [
                'run_tests' => [
                    'options' => [
                        'route' => 'run_tests',
                        'defaults' => [
                            'controller' => 'Tester\controller\Index',
                            'action' => 'index'
                        ]
                    ]
                ]
            ]
        ]
    ],
    'tests' => [
        'errors_receivers' => [
            'velchenko@360lab.com'
        ]
    ],
    'view_manager' => [
        'template_map' => [
            'test/report' => ROOT_PATH . '/tests/_output/report.html',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

];