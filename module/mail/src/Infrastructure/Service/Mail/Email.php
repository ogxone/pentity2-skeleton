<?php
/**
 * Unjudder Mail Module on top of Zendframework 2
 *
 * @link http://github.com/unjudder/zf2-mail for the canonical source repository
 * @copyright Copyright (c) 2012 unjudder
 * @license http://unjudder.com/license/new-bsd New BSD License
 * @package Uj\Mail
 */
namespace Mail\Infrastructure\Service\Mail;

use Zend\I18n\Translator\Translator;
use Zend\Mail\Message;
use Zend\Mail\Transport\TransportInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\RendererInterface;

/**
 * Uj\Mail email service class.
 *
 * @since 1.0
 * @package Uj\Mail\Service
 */
class Email
{
    /**
     * The mail transport
     *
     * @var TransportInterface
     */
    protected $_transport = null;

    /**
     * The email renderer.
     *
     * @var RendererInterface
     */
    protected $_renderer = null;

    /**
     * @var Translator
    */
    protected $_translator = null;

    /**
     * Initialize the mail service
     *
     * @param TransportInterface $transport
     * @param RendererInterface $renderer
     * @param Translator $translator
     */
    public function __construct(TransportInterface $transport, RendererInterface $renderer, Translator $translator)
    {
        $this->_transport = $transport;
        $this->_renderer = $renderer;
        $this->_translator = $translator;
    }

    /**
     * Sends an email.
     *
     * @param string|Message $tpl
     * @param array          $data
     */
    public function send($tpl, Array $data = null)
    {
        if ($tpl instanceof Message) {
            $mail = $tpl;
        } else {
            if ($data === null) {
                throw new \InvalidArgumentException('Expected data to be array, null given.');
            }

            $mail = $this->getMessage($tpl, $data);
        }

        $this->getTransport()->send($mail);
    }

    /**
     * @param  string  $tpl
     * @param  array   $data
     * @return Message
     */
    public function getMessage($tpl, Array $data)
    {
        $mail = new Message();

        if (isset($data['encoding'])) {
            $mail->setEncoding($data['encoding']);
        }
        if (isset($data['from'])) {
            $mail->setFrom($data['from']);
        }
        if (isset($data['to'])) {
            $mail->setTo($data['to']);
        }
        if (isset($data['cc'])) {
            $mail->setCc($data['cc']);
        }
        if (isset($data['bcc'])) {
            $mail->setBcc($data['bcc']);
        }
        if (isset($data['subject'])) {
            $mail->setSubject($this->_translator->translate($data['subject']));
        }
        if (isset($data['sender'])) {
            $mail->setSender($data['sender']);
        }
        if (isset($data['replyTo'])) {
            $mail->setReplyTo($data['replyTo']);
        }

        $content = $this->renderMail($tpl, $data);
        $mail->setBody($content);
        $mail->getHeaders()
            ->addHeaderLine('Content-Type', 'text/plain; charset=UTF-8')
            ->addHeaderLine('Content-Transfer-Encoding', '8bit');

        return $mail;
    }

    /**
     * Returns the mail transport
     *
     * @return \Zend\Mail\Transport\TransportInterface
     */
    public function getTransport()
    {
        return $this->_transport;
    }

    /**
     * Sets the transport
     *
     * @param TransportInterface $transport
     */
    public function setTransport(TransportInterface $transport)
    {
        $this->_transport = $transport;
    }

    /**
     * @return \Zend\View\Renderer\RendererInterface
     */
    public function getRenderer()
    {
        return $this->_renderer;
    }

    /**
     * @param \Zend\View\Renderer\RendererInterface $renderer
     */
    public function setRenderer(RendererInterface $renderer)
    {
        $this->_renderer = $renderer;
    }

    /**
     * Render a given template with given data assigned.
     *
     * @param  string $tpl
     * @param  array  $data
     * @return string The rendered content.
     */
    protected function renderMail($tpl, array $data)
    {
        $viewModel = new ViewModel($data);
        $viewModel->setTemplate($tpl);

        return $this->_renderer->render($viewModel);
    }
}
