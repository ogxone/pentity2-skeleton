<?php
/**
 * Unjudder Mail Module on top of Zendframework 2
 *
 * @link http://github.com/unjudder/zf2-mail for the canonical source repository
 * @copyright Copyright (c) 2012 unjudder
 * @license http://unjudder.com/license/new-bsd New BSD License
 * @package Uj\Mail
 */
return [
    'mail' => [
        'transport' => [
            'type' => 'sendmail',
            'options' => []
        ],
        'renderer' => [
            'templateMap' => [
                'mail/login/restore_password' => __DIR__ . '/../view/login/restore_password.phtml',
                'mail/login/change_email' => __DIR__ . '/../view/login/restore_password.phtml'
            ],
            'templatePathStack' => [
                __DIR__ . '/../view',
            ]
        ]
    ],
    'service_manager' => [
        'factories' => [
            'Mail\Email' => 'Mail\Infrastructure\Service\Mail\Factory\EmailFactory',
            'Mail\Renderer' => 'Mail\Infrastructure\Service\Mail\Factory\RendererFactory',
            'Mail\Transport' => 'Mail\Infrastructure\Service\Mail\Factory\TransportFactory'
        ]
    ]
];
