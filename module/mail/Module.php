<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 03.05.2015
 * Time: 22:12
 */

namespace Mail;


use Pentity2\Infrastructure\Mvc\Module\AbstractModule;

class Module extends AbstractModule
{
    public function getConfig()
    {
        return $this->_mergeTemplateMapToConfig(
            include __DIR__ . '/config/module.config.php',
            __DIR__ . '/template_map.php'
        );
    }
} 