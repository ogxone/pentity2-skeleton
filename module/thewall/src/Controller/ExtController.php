<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 19:03
 */

namespace Thewall\Controller;


use Pentity2\Infrastructure\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ExtController extends AbstractActionController
{
    public function indexAction()
    {
        $content = $this->getServiceLocator()->get('ViewRenderer')->render('ext/thewall');
        $response = $this->getResponse();
        $response->setContent($content);
        return $response;
    }
}