<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 19:02
 */

return [
    'controllers' => [
        'invokables' => [
            'Thewall\Controller\Ext' => 'Thewall\Controller\ExtController'
        ]
    ],
    'service_manager' => [
        'factories' => [
            'Thewall\Navigation' => 'Thewall\Navigation\Factory\ThewallNavigationFactory',
        ]
    ],
    'router' => [
        'routes' => [
            'thewall' => [
                'type' => 'literal',
                'options' => [
                    'route' => '/thewall',
                    'defaults' => [
                        'controller' => 'Thewall\Controller\Ext',
                        'action' => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'backend' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => '/:controller/:action',
                            'defaults' => [
                                '__NAMESPACE__' => 'Thewall\Controller',
                            ]
                        ],
                        'constraints' => [
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        ]
                    ],
                    'module' => [
                        'type' => 'literal',
                        'options' => [
                            'route' => '/module',
                            'defaults' => [
                                'controller' => 'Thewall\Controller\Ext',
                                'action' => 'index'
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'wildcard' => [
                                'type' => 'wildcard',
                                'options' => [
                                    'defaults' => [
                                        'controller' => 'Thewall\Controller\Ext',
                                        'action' => 'index'
                                    ],
                                ]
                            ]
                        ]
                    ],
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'template_map' => [
            'layout/thewall' => __DIR__ . '/../view/layout/layout.phtml',
            'ext/thewall' => PUBLIC_PATH . '/twdesign/index.php'
        ],
    ]
];