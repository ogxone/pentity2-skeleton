<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 20:38
 */

namespace Index;

use Pentity2\Domain\Service\Builder\Assembler\AbstractAssembler;
use Pentity2\Infrastructure\Mvc\Module\AbstractModule;
use Pentity2\Infrastructure\Repo\AbstractRepo;
use Utils\Param\Param;
use Zend\EventManager\EventManager;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;
use Zend\Session\Container;

class Module extends AbstractModule
{
    public function getConfig()
    {
        $c = $this->_mergeTemplateMapToConfig(
            include __DIR__ . '/config/module.config.php',
            __DIR__ . '/template_map.php'
        );
        return $c;
    }

    public function onBootstrap(MvcEvent $e)
    {
        $this->_applyPhpSettings($e);
        $this->_setSessionManager($e);
        $this->_setAfterRouteServices($e);
    }

    public function _setAfterRouteServices(MvcEvent $e)
    {
        // services , which have to be initialized after routing
        $e->getApplication()
            ->getEventManager()
            ->attach(MvcEvent::EVENT_DISPATCH, function(MvcEvent $e){
                $this->_setDbErrorHandler($e);
                $routeMatch = $e->getRouteMatch();
                if ($routeMatch && 0 !== strpos($routeMatch->getParam('controller'), 'Thewall')) {
                    $this->_setCaching($e);
                }
            }, 2);

    }

    private function _setDbErrorHandler(MvcEvent $e)
    {
        $sl = $e->getApplication()
            ->getServiceManager();
        $dbErrorPlugin = $sl->get('Index\DbErrorPlugin');
        $sl->get('SharedEventManager')
            ->attachAggregate($dbErrorPlugin);
    }

    private function _setSessionManager(MvcEvent $e)
    {
        $manager = $e->getApplication()
            ->getServiceManager()
            ->get('App\SessionManager');
        Container::setDefaultManager($manager);
        $manager->start();
    }

    private function _applyPhpSettings(MvcEvent $e)
    {
        $config = $e->getApplication()
            ->getServiceManager()
            ->get('Config');
        foreach (Param::getArr('php_settings', $config) as $name => $value) {
            ini_set($name, $value);
        }
    }

    private function _setCaching(MvcEvent $e)
    {
        /**
         * @var $sl ServiceManager
         * @var $em EventManager
         */
        $sl = $e->getApplication()->getServiceManager();
        $em = $e->getApplication()->getEventManager();
        //db caching
        AbstractRepo::setUseCaching(USE_CACHING);
        //builder caching
        AbstractAssembler::setUseCaching(USE_CACHING);
        //caching
        if (USE_CACHING) {
            //page and route caching
            $em->attach($sl->get('Index\RouterCachePlugin'));
            $em->attach($sl->get('Index\PageCachePlugin'));
            //translator caching
            $sl->get('App\Translator')->setCache($sl->get('Cache\Translator')); //@todo test this
            //acl caching
            $allowOverride = $sl->getAllowOverride();
            $sl->setAllowOverride(1);
            $sl->setService('App\Acl', $sl->get('App\CachingAcl'));
            $sl->setAllowOverride($allowOverride);
            //@todo navigation cache
        }
    }
} 