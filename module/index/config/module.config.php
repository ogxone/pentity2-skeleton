<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 20:38
 */
return [
    'service_manager' => [
        'invokables' => [
            'MCA' => 'Pentity2\Infrastructure\Service\MCA\MCAService'
        ],
        'factories' => [
            'CDN' => 'Pentity2\Infrastructure\Service\CDN\Factory\CDNFactory',
            'Index\Navigation' => 'Index\Navigation\Factory\IndexNavigationFactory',
            'Index\DbErrorPlugin' => 'Index\Application\Service\DbErrorPlugin\Factory\DbErrorPluginServiceFactory',
            'Index\RouterCachePlugin' => 'Index\Application\Service\RouteCache\Factory\RouteCacheFactory',
            'Index\PageCachePlugin' => 'Index\Application\Service\PageCache\Factory\PageCacheFactory'
        ]
    ],

    'controllers' => [
        'invokables' => [
            'Index\Controller\Index' => 'Index\Controller\IndexController'
        ]
    ],
    'router' => [
        'routes' => [
            'home' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => 'Index\Controller\Index',
                        'action' => 'index',
//                        'page-cacheable' => 1
                    ]
                ],
            ],
            'load_models' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/load-models/:id',
                    'defaults' => [
                        'controller' => 'Index\Controller\Index',
                        'action' => 'load-models'
                    ]
                ]
            ],
        ]
    ],
    'assets_options' => [
        'exclude_main_modules' => ['thewall']
    ],
    'assets' => [
        // only default allowed for ajax site
        'main' => [
            'js' => [
                ['path' => '/design/js/jquery-2.1.3.min.js'],
                ['path' => '/design/js/jquery.pjax.js'],
                ['path' => '/design/js/purl.js'],
                ['path' => '/design/js/underscore-min.js'],
                ['path' => '/design/js/modules/ajax/navigation'],
                ['path' => '/design/js/jquery.plugins.js'],
                ['path' => '/design/js/jquery.form.js'],
                ['path' => '/design/js/jquery.validate.js'],
                ['path' => '/design/js/jquery.validate.config.js'],
                ['path' => '/design/js/grid.js', 'vendor_path' => VENDOR_PATH . '/pentity2/pentity-grid/assets/grid.js'],
                ['path' => '/design/js/modules/ajax/cmp'],
                ['path' => '/design/js/modules/core/mediator.js'],
                ['path' => '/design/js/modules/ajax/utils/utils.js'],
                ['path' => '/design/js/ui/jquery-ui.min.js'],
                ['path' => '/design/js/init.js'],
                ['path' => '/design/js/pages.js']
            ],
            'css' => [
                ['path' => '/design/css/main'],
                ['path' => '/design/css/ui/jquery-ui.min.css'],
            ]
        ],
    ],
    'navigation' => [
        'Index\Navigation' => [
            [
                'label' => 'home',
                'route' => 'home',
            ],
            [
                'label' => 'login',
                'route' => 'auth/login',
                'resource' => 'auth_index_index',
            ],
            [
                'label' => 'register',
                'route' => 'register',
                'resource' => 'auth_index_index',
            ],
            [
                'label' => 'profile',
                'route' => 'auth/change-pass',
                'resource'=> 'auth_change',
                'pages' => [
                    [
                        'label' => 'change_pass',
                        'route' => 'auth/change-pass',
                    ],
                    [
                        'label' => 'change_email',
                        'route' => 'auth/change-email',
                    ],
                ]
            ],
            [
                'label' => 'Logout',
                'id' => 'logout',
                'route' => 'logout',
                'resource' => 'auth_index_logout',
            ],
        ]
    ],
    'view_helpers' => [
        'factories' => [
            'CDN' => 'Pentity2\Infrastructure\Mvc\View\Helper\CDN\Factory\CDNFactory',
            'MCA' => 'Pentity2\Infrastructure\Mvc\View\Helper\MCA\Factory\MCAFactory',
            'lang' => 'Pentity2\Infrastructure\Mvc\View\Helper\Lang\Factory\LangFactory',
            'translate' => 'Pentity2\Infrastructure\Mvc\View\Helper\Translate\Factory\TranslateFactory',
            'rormRow' => 'Pentity2\Infrastructure\Mvc\View\Helper\FormRow\Factory\FormRowFactory',
            'params' => 'Pentity2\Infrastructure\Mvc\View\Helper\Params\Factory\ParamsFactory',
            'config' => 'Pentity2\Infrastructure\Mvc\View\Helper\Config\Factory\ConfigFactory',
        ],
        'invokables' => [
            'formElement' => 'Pentity2\Infrastructure\Mvc\Forms\View\Helper\FormElementHelper',
            'formTag' => 'Pentity2\Infrastructure\Mvc\Forms\View\Helper\FormTagHelper',
            'list' => 'Pentity2\Infrastructure\Mvc\View\Helper\Grid\ListViewHelper',
            'grid' => 'Pentity2\Infrastructure\Mvc\View\Helper\Grid\GridViewHelper',
            'paging' => 'Pentity2\Infrastructure\Mvc\View\Helper\Pagination\PaginationHelper',
            'linkPager' => 'Pentity2\Infrastructure\Mvc\View\Helper\Pagination\LinkPagerHelper',
            'messages' => 'Pentity2\Infrastructure\Mvc\View\Helper\Messages\MessageHelper',
            'formErrors' => 'Pentity2\Infrastructure\Mvc\View\Helper\FormErrors\FormErrorsHelper'
        ]
    ],
    'view_manager' => [
        'doctype' => 'HTML5',
        'display_not_found_reason' => true,
        'display_exceptions' => ENVIRONMENT != 'production',
        'exception_template' => 'error/index',
        'not_found_template' => 'error/404',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ],
    ],
    'log' => [
        'Log\DbError' => [
            'writers' => [
                [
                    'name' => 'stream',
                    'priority' => 1000,
                    'options' => [
                        'stream' => DATA_PATH . '/logs/db_error.log'
                    ]
                ]
            ]
        ]
    ],
];