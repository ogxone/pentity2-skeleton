<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 07.09.15
 * Time: 11:39
 */

namespace Index\Application\Service\RouteCache\Factory;


use Index\Application\Service\RouteCache\RouteCacheService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RouteCacheFactory  implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new RouteCacheService($serviceLocator->get('Cache\Routes'));
    }
}