<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.09.15
 * Time: 14:01
 */

namespace Index\Application\Service\RouteCache;


use Pentity2\Infrastructure\Cache\StorageInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\Http\Literal;

class RouteCacheService implements ListenerAggregateInterface
{
    const ROUTE_CACHE_TAG = 'route_cache';

    private $_listeners = [];
    private $_cache;

    public function __construct(StorageInterface $cache)
    {
        $this->_cache = $cache;
    }

    public function attach(EventManagerInterface $events)
    {
        $this->_listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, [$this, 'routeLoad'], 999);
        $this->_listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, [$this, 'routeSave'], 0);
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->_listeners as $index => $callback) {
            if ($events->detach($callback)) {
                unset($this->_listeners[$index]);
            }
        }
    }

    public function routeSave(MvcEvent $event)
    {
        $match = $event->getRouteMatch();
        if(!$match) {
            return;
        }

        if($event->getParam('route-cached') || !$event->getParam('route-cacheable')) {
            return;
        }

        $path = $event->getRequest()
            ->getUri()
            ->getPath();

        // save the route match into the cache.
        $data = array (
            'name' => $event->getRouteMatch()->getMatchedRouteName(),
            'route' => $path,
            'defaults' => $event->getRouteMatch()->getParams(),
        );
        $this->_cache->setItem($this->_createName($path), $data, [self::ROUTE_CACHE_TAG]);
    }

    public function routeLoad(MvcEvent $event)
    {
        /**@va $request Request*/
        $request = $event->getRequest();
        if(!(
            $request->getMethod() == Request::METHOD_GET &&
            $request->getQuery()->count() == 0
        )) {
            // We do not cache route match for requests that can produce
            // different match.
            return ;
        }

        $event->setParam('route-cacheable', true);

        // check if we have data in our cache
        $path = $event->getRequest()
            ->getUri()
            ->getPath();

        $cachedData = $this->_cache->getItem($this->_createName($path));

        if(!empty($cachedData)) {
            $event->setParam('route-cached', true);

            $cachedRoute = Literal::factory($cachedData);
            $router = $event->getRouter();
            $router->addRoute($cachedData['name'], $cachedRoute, 99999);
        }
    }

    private function _createName($path)
    {
        return 'route-cache-' . preg_replace('/\s/', '-', str_replace('/', '-', $path));
    }
}