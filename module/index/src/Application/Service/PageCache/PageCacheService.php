<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 07.09.15
 * Time: 13:18
 */

namespace Index\Application\Service\PageCache;



use Pentity2\Infrastructure\Cache\StorageInterface;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;


class PageCacheService extends AbstractListenerAggregate
{
    const PAGE_CACHE_TAG = 'page_cache';

    protected $_listeners = [];
    protected $_cache;

    public function __construct(StorageInterface $cache)
    {
        $this->_cache = $cache;
    }

    public function attach(EventManagerInterface $events)
    {
        $this->_listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'getCache'), -1000);
        $this->_listeners[] = $events->attach(MvcEvent::EVENT_RENDER, array($this, 'saveCache'), -10000);
    }

    public function getCache(MvcEvent $event)
    {
        $match = $event->getRouteMatch();

        // is valid route?
        if (!$match) {
            return;
        }

        // does our route have the cache flag set to true?
        if ($match->getParam('page-cacheable')) {
            $cacheKey = $this->_createName($match);

            // get the cache page for this route
            $data = $this->_cache->getItem($cacheKey);

            // ensure we have found something valid
            if (null !== $data) {
                $response = $event->getResponse();
                $response->setContent($data);

                return $response;
            }
        }
    }

    public function saveCache(MvcEvent $event)
    {
        $match = $event->getRouteMatch();

        // is valid route?
        if (!$match) {
            return;
        }

        // does our route have the cache flag set to true?
        if ($match->getParam('page-cacheable')) {
            $response = $event->getResponse();
            $data = $response->getContent();

            $cacheKey = $this->_createName($match);
            $this->_cache->setItem($cacheKey, $data, [self::PAGE_CACHE_TAG]);
        }
    }


    private function _createName(RouteMatch $match)
    {
        return 'cache-pages-' . str_replace('/', '-', $match->getMatchedRouteName() . '-' . md5(serialize($match->getParams())));
    }
}


