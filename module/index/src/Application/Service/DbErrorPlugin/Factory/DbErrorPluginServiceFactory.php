<?php

/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 18.08.15
 * Time: 20:24
 */
namespace Index\Application\Service\DbErrorPlugin\Factory;

use Index\Application\Service\DbErrorPlugin\DbErrorPluginService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DbErrorPluginServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new DbErrorPluginService($serviceLocator->get('Log\DbError'));
    }
}