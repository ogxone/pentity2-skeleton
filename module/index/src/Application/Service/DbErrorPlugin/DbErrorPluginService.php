<?php

/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.05.2015
 * Time: 21:33
 */

namespace Index\Application\Service\DbErrorPlugin;

use Pentity2\Infrastructure\Mapper\AbstractMapper;
use Zend\EventManager\EventInterface;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\Log\Logger;

class DbErrorPluginService implements SharedListenerAggregateInterface
{
    private $_logger;
    private $_listeners;

    public function __construct(Logger $logger)
    {
        $this->_logger = $logger;
    }

    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->_listeners = $events->attach(AbstractMapper::IDENTIFIER, AbstractMapper::EVENT_BEFORE_EXECUTE, [$this, 'beforeExecute']);
        $this->_listeners = $events->attach(AbstractMapper::IDENTIFIER, AbstractMapper::EVENT_DB_FAILURE, [$this, 'error']);
    }

    public function detachShared(SharedEventManagerInterface $events)
    {
        foreach ($this->_listeners as $index => $callback) {
            if ($events->detach(AbstractMapper::IDENTIFIER, $callback)) {
                unset($this->_listeners[$index]);
            }
        }
    }

    public function beforeExecute(EventInterface $event)
    {

    }

    public function error(EventInterface $event)
    {
        $exception = $event->getParam('exception');
        $sql       = $event->getParam('query');

        $message = sprintf(
            'SQL ERROR(%s) occurred with message:%s. On query: %s',
            $exception->getCode(),
            $exception->getMessage(),
            $sql
        );
        $this->_logger->log(Logger::ERR, $message);
        throw $exception;
    }
}
