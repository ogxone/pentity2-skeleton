<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.05.2015
 * Time: 8:07
 */

namespace Index\Navigation\Factory;

use Zend\Navigation\Service\DefaultNavigationFactory;

class IndexNavigationFactory extends DefaultNavigationFactory
{
    protected function getName()
    {
        return 'Index\Navigation';
    }
}